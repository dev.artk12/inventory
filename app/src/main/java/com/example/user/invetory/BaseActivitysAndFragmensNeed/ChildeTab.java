package com.example.user.invetory.BaseActivitysAndFragmensNeed;



        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.widget.AlertDialogLayout;
        import android.support.v7.widget.DefaultItemAnimator;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.util.LruCache;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.android.volley.AuthFailureError;
        import com.android.volley.DefaultRetryPolicy;
        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;
        import com.example.user.invetory.ContentObject;
        import com.example.user.invetory.Cryptor;
        import com.example.user.invetory.DetailePage.DetailActivity;
        import com.example.user.invetory.MyAdapter;
        import com.example.user.invetory.Output_files;
        import com.example.user.invetory.R;
        import com.wang.avi.AVLoadingIndicatorView;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Map;
        import java.util.concurrent.TimeUnit;


public class ChildeTab extends Fragment{
    private static final String ARG_PARAM1 = "group_name";


    private String Group_Name;
    String email ;
    private static String ID_KEY            =   "ID";
    private static String NAME_KEY          =   "Name";
    private static String STOCK_KEY         =   "Stock";
    private static String ENCASEMENT_KEY    =   "Encasement" ;
    private static String BARCODE_KEY       =   "BarCode" ;
    private static String IMAGENAMR_KEY     =   "ImageName" ;
    private static String STOCKFORMAT_KEY   =   "StockFormat" ;
    private static String SALE_KEY          =   "Sale_Price" ;
    private static String SALEFORMAT_KEY    =   "Sale_PriceFormat" ;
    private static String KEY = "afsaneh";

    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    MyAdapter adapter;
    ArrayList<ContentObject> objects;
    View view;
    Output_files files;
    Cryptor cryptor;
    AVLoadingIndicatorView avi;
    Animation animation;
    AlertDialog.Builder dialog;
    RequestQueue queue;
    StringRequest request;

    public static ChildeTab newInstance(String group_Name) {
        ChildeTab fragment = new ChildeTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, group_Name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            objects = new ArrayList<>();
            Group_Name = getArguments().getString(ARG_PARAM1);
            cryptor = new Cryptor();
            dialog = new AlertDialog.Builder(getContext());


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_childe_tab, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.gruops);
        TextView tv = (TextView) view.findViewById(R.id.name);
        files = new Output_files(getContext());
        email = files.read("email");
        tv.setText(Group_Name);
        avi = (AVLoadingIndicatorView) view.findViewById(R.id.avi);
        animation = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        final int size = objects.size();
        if (size > 0) {
            objects.clear();
            adapter.notifyItemRangeRemoved(0,size);
        }
        Request(email);
    }

    private void Request( final String email) {


        String url = "http://www.inventory-customer.com/source/getAllValueFromGruop.php";

        queue = Volley.newRequestQueue(getContext());

        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length() ; i++) {
                        ContentObject object = new ContentObject();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString(NAME_KEY);
                        String stock = cryptor.Decrypt(jsonObject.getString(STOCK_KEY),KEY);
                        String barcode = cryptor.Decrypt(jsonObject.getString(BARCODE_KEY),KEY);
                        String encasement = cryptor.Decrypt(jsonObject.getString(ENCASEMENT_KEY),KEY);
                        String stockformat = cryptor.Decrypt(jsonObject.getString(STOCKFORMAT_KEY),KEY);
                        String Sale = cryptor.Decrypt(jsonObject.getString(SALE_KEY),KEY);
                        String SaleFormat = cryptor.Decrypt(jsonObject.getString(SALEFORMAT_KEY),KEY);


                        object.setID(jsonObject.getInt(ID_KEY));
                        object.setName(name);
                        object.setStock(stock);
                        object.setStockFormat(stockformat);
                        object.setBarCode(barcode);
                        object.setEncasement(encasement);
                        object.setSale_Price(Sale);
                        object.setSaleFormat(SaleFormat);
                        object.setImageAddress(jsonObject.getString(IMAGENAMR_KEY));
                        objects.add(object);
                    }
                    manager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    adapter = new MyAdapter(getContext(),objects,email);
                    recyclerView.setAdapter(adapter);

                    avi.setAnimation(animation);
                    avi.hide();


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.setMessage("Please Check Your Network").setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queue.add(request);
                    }
                }).show();


            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMap = new HashMap<>();
                stringMap.put("email",email);
                stringMap.put("name",Group_Name);
                return stringMap;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

}
