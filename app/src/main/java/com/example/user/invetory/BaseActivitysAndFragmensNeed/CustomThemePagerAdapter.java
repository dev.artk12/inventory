package com.example.user.invetory.BaseActivitysAndFragmensNeed;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.user.invetory.R;


public class CustomThemePagerAdapter extends PagerAdapter {

    int [] themes;

    public CustomThemePagerAdapter(int [] thems){
        this.themes = thems;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {return  view == object;}

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.layout_theme_child,container,false);
        ImageView img = (ImageView) view.findViewById(R.id.themes);
        img.setImageResource(themes[position]);

        container.addView(view);

        return view;
    }
}
