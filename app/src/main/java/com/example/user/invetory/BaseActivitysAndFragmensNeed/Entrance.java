package com.example.user.invetory.BaseActivitysAndFragmensNeed;




import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.invetory.CreateObject.AddObject;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.Help;
import com.example.user.invetory.Languge;
import com.example.user.invetory.LocalHelper;
import com.example.user.invetory.MainActivity;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.Profile.Profile;
import com.example.user.invetory.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import de.hdodenhof.circleimageview.CircleImageView;

public class Entrance extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,MyInterface,RecyclerCallBackTab1,RecyclerCallBackTab2,RecyclerCallBackTab3 {


    private static String KEY = "afsaneh";

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Output_files files = new Output_files(Entrance.this);
    String name , email , photo , nameEncryption , photoEncyption ;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Bitmap bitmap;
    File file;
    Class fragmentclass;
    FragmentManager fragmentManager;
    Fragment fragment;
    FragmentTransaction transaction;
    boolean close ,getfromserver ,down , up ;
    View view;
    DisplayMetrics metrics;
    int height , width;
    float height_color;
    ViewPager viewPager;
    ItemFragment section;
    TabLayout tabLayout;
    Cryptor cryptor;
    CreateTable table;
    SwitchCompat nightmode;
    AppCompatImageButton create , search , refresh;
    Animation animation;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        editor.putBoolean("tab1",false).apply();
        editor.putBoolean("tab2",false).apply();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        preferences = PreferenceManager.getDefaultSharedPreferences(Entrance.this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme", 0);
        up = true;
        down = true;


        switch (themepos) {
            case 0:
                setTheme(R.style.DefultTheme);
                break;
            case 1:
                setTheme(R.style.RedTheme);
                break;
            case 2:
                setTheme(R.style.GreenTheme);
                break;
            case 3:
                setTheme(R.style.NightMode);
                break;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrance);
        table = new CreateTable();

        navigationView = (NavigationView) findViewById(R.id.nav);
        nightmode = (SwitchCompat) navigationView.getMenu().findItem(R.id.night_mode).getActionView();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        viewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        cryptor = new Cryptor();
        create = (AppCompatImageButton) findViewById(R.id.create);
        search = (AppCompatImageButton) findViewById(R.id.ic_search);
        refresh = (AppCompatImageButton) findViewById(R.id.refresh);


        if (themepos == 3){
            nightmode.setChecked(true);
        }else {
            nightmode.setChecked(false);
        }


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });

        nightmode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("tab2",false).apply();
                editor.putBoolean("tab1",false).apply();
                if (isChecked){
                    editor.putInt("theme",3).apply();
                    startActivity(new Intent(Entrance.this,Entrance.class));
                    finish();
                }else {
                    editor.remove("theme").apply();
                    startActivity(new Intent(Entrance.this,Entrance.class));
                    finish();

                }
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("tab2",true);
                editor.putBoolean("tab1",true);
                editor.putBoolean("checktab2",false);
                editor.apply();
                onResume();
            }
        });



        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(Entrance.this, drawerLayout, toolbar , R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(Entrance.this);
        view = navigationView.inflateHeaderView(R.layout.header_drawerlayout);
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        height_color = (float) (height / 3.5);

        close = preferences.getBoolean("close MainActivity", true);
        getfromserver = preferences.getBoolean("getfromserver", false);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entrance.this, AddObject.class);
                startActivity(intent);
            }
        });




        if (close == true) {
            startActivity(new Intent(Entrance.this, MainActivity.class));
            finish();
        }
        if (close == false) {
            if (getfromserver == true) {
                Bundle bundle = getIntent().getExtras();

                if (bundle == null){
                    startActivity(new Intent(Entrance.this, MainActivity.class));
                    finish();
                }else {
                    email = bundle.getString("email");
                    String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                    File file = new File(path,"Inventory");
                    if (!file.mkdir()){
                        file.mkdir();
                    }
                    File PDFFolder = new File(file,"PDF");
                    if (!PDFFolder.mkdir()){
                        PDFFolder.mkdir();
                    }
                    File ImageFolder = new File(file,"Image");
                    if (!ImageFolder.mkdir()){
                        ImageFolder.mkdir();
                    }
                    File ExcelFolder = new File(file , "Excel");
                    if (!ExcelFolder.mkdir()){
                        ExcelFolder.mkdir();
                    }


                    table.GetUsersDetails(Entrance.this, email, new CreateTable.VollyCallbackonSuccess() {
                        @Override
                        public void onSuccessResponse(String result) {


                            try {
                                JSONObject object = new JSONObject(result);

                                nameEncryption = object.getString("CompanyName");
                                photoEncyption = object.getString("Photo");
                                email = object.getString("EmailAddress");

                                name = cryptor.Decrypt(nameEncryption, KEY);
                                photo = cryptor.Decrypt(photoEncyption, KEY);

                                files.write_prof_iamge(photo);
                                files.write_nameandlastname("name", name);
                                files.write("email", email);
                                byte[] photobytes = Base64.decode(photo, Base64.DEFAULT);
                                bitmap = BitmapFactory.decodeByteArray(photobytes, 0, photobytes.length);

                                HeaderColor(view, width, height_color);

                                Imagelocation(view, metrics, width, height_color, bitmap);

                                Namelocation(view, metrics, width, height_color, name);

                                Emaillocation(view, metrics, width, height_color, email);

                                Titlelocation(view, metrics, height_color);

                            } catch (JSONException e) {
                                Log.e("fuuuck", e.getMessage());
                            }

                        }
                    });

                }

                editor.remove("getfromserver").apply();
            }
           else if (getfromserver == false) {
                    name = files.read_nameandlastname("name");
                    email = files.read("email");
                    file = getFilesDir();
                    bitmap = BitmapFactory.decodeFile(file.getAbsolutePath() + "/profileimage.png");


                    HeaderColor(view, width, height_color);

                    Imagelocation(view, metrics, width, height_color, bitmap);

                    Namelocation(view, metrics, width, height_color, name);

                    Emaillocation(view, metrics, width, height_color, email);

                    Titlelocation(view, metrics, height_color);

                }
            }
        }



    @Override
    protected void onResume() {
        super.onResume();
        section = new ItemFragment(getSupportFragmentManager(),email) ;
        viewPager.setAdapter(section);
        viewPager.setCurrentItem(1);
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        fragment = null ;
        fragmentclass = null;
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        int id = item.getItemId();

        switch (id){

            case R.id.prof :
                finish();
                startActivity(new Intent(Entrance.this,Profile.class));
                // bitmap.recycle();
                //fragmentclass = Profile.class;
                editor.putBoolean("tab2",false).apply();
                editor.putBoolean("tab1",false).apply();

                break;
            case R.id.theme :
               fragmentclass = Theme.class;
                editor.putBoolean("tab2",false).apply();
                editor.putBoolean("tab1",false).apply();
                //drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.language :
                finish();
                startActivity(new Intent(Entrance.this,Languge.class));
                editor.putBoolean("tab2",false).apply();
                editor.putBoolean("tab1",false).apply();
                break;

            case R.id.Log_out :
                    finish();
                    startActivity(new Intent(Entrance.this,MainActivity.class));
                editor.putBoolean("tab2",true);
                editor.putBoolean("tab1",true);
                editor.putBoolean("close MainActivity",true);
                editor.apply();
                break;

            case R.id.invatefriend :
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=com.whatsapp"));
                startActivity(intent);
                editor.putBoolean("tab2",false).apply();
                editor.putBoolean("tab1",false).apply();
                break;

            case R.id.help:
                fragmentclass = Help.class;
                drawer.closeDrawer(GravityCompat.START);
                editor.putBoolean("tab2",false).apply();
                editor.putBoolean("tab1",false).apply();
                break;


        }if (fragmentclass != null){

            try {
                fragment = (Fragment) fragmentclass.newInstance();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_in);
                transaction.replace(R.id.drawer_layout,fragment);
                transaction.commit();
            }catch (InstantiationException e){
                e.printStackTrace();
            }catch (IllegalAccessException e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void lockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void unlockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void HeaderColor(View view, int width, float height_color) {
        View v = (View)view.findViewById(R.id.header_view);
        v.setLayoutParams(new RelativeLayout.LayoutParams(width,(int) (height_color)));
    }

    private void Titlelocation(View view, DisplayMetrics metrics, float height_color) {
        TextView title = (TextView)view.findViewById(R.id.title);
        float pxtitle = 10 * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        RelativeLayout.LayoutParams titleparams = (RelativeLayout.LayoutParams) title.getLayoutParams();
        titleparams.setMargins(0 , (int)  ((height_color/2) - pxtitle),0, 0);
        title.setLayoutParams(titleparams);
    }

    private void Namelocation(View view, DisplayMetrics metrics, int width, float height_color,String name) {
        TextView drawername = (TextView)view.findViewById(R.id.header_name);
        RelativeLayout.LayoutParams namelayoutparams = (RelativeLayout.LayoutParams) drawername.getLayoutParams();
        float pxemail = 10 * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        namelayoutparams.setMargins(width/30, (int) pxemail,0,0);
        drawername.setLayoutParams(namelayoutparams);
        drawername.setText(name);

    }

    private void Imagelocation(View view, DisplayMetrics metrics, int width, float height_color,Bitmap bitmap) {
        float pximg = 45 * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        CircleImageView drawerimage = (CircleImageView)view.findViewById(R.id.circleImageView);
        RelativeLayout.LayoutParams imagelayoutparams = (RelativeLayout.LayoutParams) drawerimage.getLayoutParams();
        imagelayoutparams.setMargins(width/20, (int) (height_color-pximg),0,0);
        drawerimage.setLayoutParams(imagelayoutparams);
        drawerimage.setImageResource(R.drawable.layer);
        if (bitmap != null){
            drawerimage.setImageBitmap(bitmap);
        }
    }

    private void Emaillocation(View view, DisplayMetrics metrics, int width, float height_color,String email) {
        TextView draweremail = (TextView)view.findViewById(R.id.header_email);
        RelativeLayout.LayoutParams emaillayoutparams = (RelativeLayout.LayoutParams) draweremail.getLayoutParams();
        emaillayoutparams.setMargins(width/30, 0 , 0 , 0);
        draweremail.setLayoutParams(emaillayoutparams);
        draweremail.setText(email);
    }

    @Override
    public void RecyclerViewCallBackTab1(RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && down ){
                    animation = AnimationUtils.loadAnimation(Entrance.this,R.anim.tranclatedown);
                    create.setAnimation(animation);
                    create.setVisibility(View.INVISIBLE);
                    down = false;
                }else if (dy < 0 && up){
                    animation = AnimationUtils.loadAnimation(Entrance.this,R.anim.tranclateup);
                    create.setAnimation(animation);
                    create.setVisibility(View.VISIBLE);
                    down = true;
                }
            }
        });

    }

    @Override
    public void RecyclerViewCallBackTab2(RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && down ){
                    animation = AnimationUtils.loadAnimation(Entrance.this,R.anim.tranclatedown);
                    create.setAnimation(animation);
                    create.setVisibility(View.INVISIBLE);
                    down = false;
                }else if (dy < 0 && up){
                    animation = AnimationUtils.loadAnimation(Entrance.this,R.anim.tranclateup);
                    create.setAnimation(animation);
                    create.setVisibility(View.VISIBLE);
                    down = true;
                }
            }
        });

    }

    @Override
    public void RecyclerViewCallBackTab3(RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && down ){
                    animation = AnimationUtils.loadAnimation(Entrance.this,R.anim.tranclatedown);
                    create.setAnimation(animation);
                    create.setVisibility(View.INVISIBLE);
                    down = false;
                }else if (dy < 0 && up){
                    animation = AnimationUtils.loadAnimation(Entrance.this,R.anim.tranclateup);
                    create.setAnimation(animation);
                    create.setVisibility(View.VISIBLE);
                    down = true;
                }
            }
        });

    }

    public static class ItemFragment extends FragmentPagerAdapter{

        String email;

        public ItemFragment(FragmentManager fm , String email) {
            super(fm); this.email = email;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 : Tab3 tab3 = Tab3.newInsatance(email); return tab3;
                case 1 : Tab1 tab1 = Tab1.newInsatance(email); return tab1;
                case 2 : Tab2 tab2 = Tab2.newInsatance(email); return tab2;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


}

interface MyInterface {
    public void lockDrawer();
    public void unlockDrawer();
}
