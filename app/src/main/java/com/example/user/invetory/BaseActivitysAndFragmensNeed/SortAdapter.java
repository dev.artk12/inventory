package com.example.user.invetory.BaseActivitysAndFragmensNeed;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.user.invetory.ContentObject;
import com.example.user.invetory.DetailePage.DetailActivity;
import com.example.user.invetory.R;

import java.util.ArrayList;

public class SortAdapter extends RecyclerView.Adapter<SortAdapter.ViewHolde> {

    ArrayList<ContentObject> objects;
    Context context;

    public SortAdapter (ArrayList<ContentObject> objects , Context context){
        this.objects = objects;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolde onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sortadapter,viewGroup,false);
        ViewHolde viewHolder = new ViewHolde(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolde viewHolde, final int i) {
        viewHolde.name.setText(objects.get(i).getName());
        StringBuilder builder = new StringBuilder(objects.get(i).getCalende());
        builder.insert(4,"/");
        builder.insert(7,"/");
        viewHolde.calendar.setText(builder.toString());

        viewHolde.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,DetailActivity.class);
                intent.putExtra("id",objects.get(i).getID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    protected class ViewHolde extends RecyclerView.ViewHolder{

        public RelativeLayout parent ;
        public AppCompatTextView calendar,name;
        public ViewHolde(@NonNull View itemView) {
            super(itemView);
            parent = (RelativeLayout) itemView.findViewById(R.id.sortparent);
            calendar = (AppCompatTextView) itemView.findViewById(R.id.calendar);
            name = (AppCompatTextView) itemView.findViewById(R.id.name);
        }

    }
}
