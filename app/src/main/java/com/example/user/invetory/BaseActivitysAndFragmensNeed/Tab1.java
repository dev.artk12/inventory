package com.example.user.invetory.BaseActivitysAndFragmensNeed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.invetory.ContentObject;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.MyAdapter;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;

import com.example.user.invetory.DetailePage.DetailActivity;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class Tab1 extends Fragment implements TochHelperListener {

    RecyclerView recyclerView;
    MyAdapter adapter;
    RecyclerView.LayoutManager manager;
    ArrayList<ContentObject> objects;
    View view;
    Animation animation;
    Boolean down , up;
    Output_files files;
    RecyclerCallBackTab1 callBack;
    String Email;
    Cryptor cryptor;
    AVLoadingIndicatorView avi;
    Animation recycleranim ;
    Animation avianim;
    StringRequest request;
    RequestQueue queue;




    String email ;
    private static String ID_KEY            =   "ID";
    private static String NAME_KEY          =   "Name";
    private static String STOCK_KEY         =   "Stock";
    private static String CREATOR_KEY       =   "Creator" ;
    private static String MODEL_KEY         =   "Model" ;
    private static String GROUPNAME_KEY     =   "Group_Name" ;
    private static String PURCHASEPRICE_KEY =   "Purchase_Price" ;
    private static String SALEPRICE_KEY     =   "Sale_Price";
    private static String DESCRIPTION_KEY   =   "Description" ;
    private static String ENCASEMENT_KEY    =   "Encasement" ;
    private static String BARCODE_KEY       =   "BarCode" ;
    private static String IMAGENAMR_KEY     =   "ImageName" ;
    private static String STOCKFORMAT_KEY   =   "StockFormat" ;
    private static String SALE_KEY          =   "Sale_Price" ;
    private static String SALEFORMAT_KEY    =   "Sale_PriceFormat" ;

    private static String KEY = "afsaneh";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    private static int TAG = 0;

    AlertDialog.Builder dialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (RecyclerCallBackTab1) context ;

    }

    public static Tab1 newInsatance(String email){
        Tab1 tab1 = new Tab1();
        Bundle bundle = new Bundle();
        bundle.putString("email",email);
        tab1.setArguments(bundle);

        return tab1;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab1,container,false);
        objects = new ArrayList<>();
        dialog = new AlertDialog.Builder(getContext());

        preference = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preference.edit();
        boolean Download = preference.getBoolean("tab1",true);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        files = new Output_files(getContext());
        Email = getArguments().getString("email");
        cryptor = new Cryptor();
        avi = view.findViewById(R.id.avi);
        avi.show();
        recycleranim = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
        avianim = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);

        callBack.RecyclerViewCallBackTab1(recyclerView);

        handleSSLHandshake();
        boolean download = preference.getBoolean("tab1",true);

        if (download){
            Download(Email);
        }else if (!download){

            objects = new ArrayList<>();
            String response = files.read("tab1");
            if (response != null){

                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length() ; i++) {
                        ContentObject object = new ContentObject();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString(NAME_KEY);
                        String stock = cryptor.Decrypt(jsonObject.getString(STOCK_KEY),KEY);
                        String barcode = cryptor.Decrypt(jsonObject.getString(BARCODE_KEY),KEY);
                        String encasement = cryptor.Decrypt(jsonObject.getString(ENCASEMENT_KEY),KEY);
                        String stockformat = cryptor.Decrypt(jsonObject.getString(STOCKFORMAT_KEY),KEY);
                        String Sale = cryptor.Decrypt(jsonObject.getString(SALE_KEY),KEY);
                        String SaleFormat = cryptor.Decrypt(jsonObject.getString(SALEFORMAT_KEY),KEY);


                        object.setID(jsonObject.getInt(ID_KEY));
                        object.setName(name);
                        object.setStockFormat(stockformat);
                        object.setStock(stock);
                        object.setSale_Price(Sale);
                        object.setSaleFormat(SaleFormat);

                        object.setBarCode(barcode);
                        object.setEncasement(encasement);
                        object.setImageAddress(jsonObject.getString(IMAGENAMR_KEY));
                        objects.add(object);

                    }

                    manager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    adapter = new MyAdapter(getContext(),objects,email);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setAnimation(recycleranim);

                    avi.setAnimation(avianim);
                    avi.hide();

                    ItemTouchHelper.SimpleCallback item = new TochHelper(ItemTouchHelper.UP|ItemTouchHelper.DOWN,ItemTouchHelper.LEFT, Tab1.this);
                    new ItemTouchHelper(item).attachToRecyclerView(recyclerView);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }else{
                Download(Email);
            }

        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        objects = new ArrayList<>();
    }

    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Download(final String email) {

        String url = "http://www.inventory-customer.com/source/getAllValue.php";

        queue = Volley.newRequestQueue(getContext());
        objects = new ArrayList<>();
        objects.clear();

        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            ContentObject object = new ContentObject();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String name = jsonObject.getString(NAME_KEY);
                            String stock = cryptor.Decrypt(jsonObject.getString(STOCK_KEY), KEY);
                            String barcode = cryptor.Decrypt(jsonObject.getString(BARCODE_KEY), KEY);
                            String encasement = cryptor.Decrypt(jsonObject.getString(ENCASEMENT_KEY), KEY);
                            String stockformat = cryptor.Decrypt(jsonObject.getString(STOCKFORMAT_KEY), KEY);
                            String Sale = cryptor.Decrypt(jsonObject.getString(SALE_KEY), KEY);
                            String SaleFormat = cryptor.Decrypt(jsonObject.getString(SALEFORMAT_KEY), KEY);


                            object.setID(jsonObject.getInt(ID_KEY));
                            object.setName(name);
                            object.setStockFormat(stockformat);
                            object.setStock(stock);
                            object.setSale_Price(Sale);
                            object.setSaleFormat(SaleFormat);
                            object.setBarCode(barcode);
                            object.setEncasement(encasement);
                            object.setImageAddress(jsonObject.getString(IMAGENAMR_KEY));

                            objects.add(object);

                        }

                    manager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    adapter = new MyAdapter(getContext(),objects,email);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setAnimation(recycleranim);
                    files.write_nameandlastname("tab1",response);


                    avi.setAnimation(avianim);
                    avi.hide();


                    ItemTouchHelper.SimpleCallback item = new TochHelper(ItemTouchHelper.UP|ItemTouchHelper.DOWN,ItemTouchHelper.LEFT, Tab1.this);
                    new ItemTouchHelper(item).attachToRecyclerView(recyclerView);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleSSLHandshake();
                objects = new ArrayList<>();
                request.setShouldCache(false);
                request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(request);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMap = new HashMap<>();
                stringMap.put("email",email);
                return stringMap;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    @Override
    public void onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

    }

}
