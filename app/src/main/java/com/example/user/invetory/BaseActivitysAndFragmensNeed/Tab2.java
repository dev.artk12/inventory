package com.example.user.invetory.BaseActivitysAndFragmensNeed;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Tab2 extends Fragment{


    private static String GROUPNAME_KEY = "Group_Name" ;
    private static String KEY = "afsaneh";
    RecyclerCallBackTab2 callBack;


    ArrayList<String> list ;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    GroupValuesAdapter groupValuesAdapter;
    String Email;
    Output_files files ;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Bundle bundle;

    public static Tab2 newInsatance(String email){
        Tab2 tab2 = new Tab2();
        Bundle bundle = new Bundle();
        bundle.putString("email",email);
        tab2.setArguments(bundle);

        return tab2;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (RecyclerCallBackTab2) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2,container,false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        files = new Output_files(getContext());
        Email = getArguments().getString("email");
        recyclerView = (RecyclerView) view.findViewById(R.id.groups);
        callBack.RecyclerViewCallBackTab2(recyclerView);
        list = new ArrayList<String>();
        boolean download = preferences.getBoolean("tab2" , true);

        if (download){
            setup(Email,list, getContext(), new GroupNameCallBack() {
                @Override
                public void GroupNameCallBack(ArrayList list) {

                    groupValuesAdapter = new GroupValuesAdapter((AppCompatActivity) getActivity(),list);
                    manager = new GridLayoutManager(getActivity(),2);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(groupValuesAdapter);

                }
            });
        }else if (!download){
            String response = files.read("tab2");
            list = new ArrayList<>();
            try {
                if (response != null){
                    
                JSONArray jsonArray = new JSONArray(response);
                if(jsonArray != null){

                    for (int i = 0; i < jsonArray.length() ; i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        list.add(object.getString(GROUPNAME_KEY));
                    }
                    groupValuesAdapter = new GroupValuesAdapter((AppCompatActivity) getActivity(),list);
                    manager = new GridLayoutManager(getActivity(),2);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(groupValuesAdapter);
                }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return view;
    }
    private static void setup(final String Email , final ArrayList<String> list, final Context context , final GroupNameCallBack callBack){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://www.inventory-customer.com/source/getgruops.php";


        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            public Output_files files = new Output_files(context);

            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length() ; i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        list.add(object.getString(GROUPNAME_KEY));
                    }
                    callBack.GroupNameCallBack(list);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                files.write_nameandlastname("tab2",response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                        map.put("email",Email);
                return map;
            }
        };

        queue.add(request);
    }
}
interface GroupNameCallBack{
        void GroupNameCallBack(ArrayList list);
    }
    /*
    recyclerView.addOnItemTouchListener(
                        new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                //ChildeTab childeTab = ChildeTab.newInstance((String) list.get(position));
                                //Toast.makeText(getContext(), ""+list.get(position), Toast.LENGTH_SHORT).show();
                                //getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content,childeTab).addToBackStack(null).commit();
                            }
                        })
                );
     */
