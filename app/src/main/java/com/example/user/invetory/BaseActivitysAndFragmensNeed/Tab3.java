package com.example.user.invetory.BaseActivitysAndFragmensNeed;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.example.user.invetory.ContentObject;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.MyAdapter;
import com.example.user.invetory.R;
import com.wang.avi.AVLoadingIndicatorView;

import net.igenius.customcheckbox.CustomCheckBox;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class Tab3 extends Fragment implements TochHelperListener {

    RecyclerView recyclerView;
    MyAdapter adapter;
    SortAdapter sortAdapter;
    RecyclerView.LayoutManager manager;
    ArrayList<ContentObject> objects;
    View view;
    Animation animation;
    RecyclerCallBackTab3 callBack;
    String Email;
    Cryptor cryptor;
    AVLoadingIndicatorView avi;
    Animation recycleranim ;
    AppCompatImageButton search , choose  , excel , sort;
    AppCompatEditText searchfild;
    RequestQueue queue;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int color;
    TextView byname,bycelender;
    LinearLayoutCompat parentby;
    StringRequest request , calendarrequest;
    RelativeLayout sortparent;
    CustomCheckBox upper,lower;
    TSnackbar tSnackbar;

    private static String ID_KEY             =   "ID";
    private static String NAME_KEY           =   "Name";
    private static String STOCK_KEY          =   "Stock";
    private static String ENCASEMENT_KEY     =   "Encasement" ;
    private static String BARCODE_KEY        =   "BarCode" ;
    private static String IMAGENAMR_KEY      =   "ImageName" ;
    private static String STOCKFORMAT_KEY    =   "StockFormat" ;
    private static String SALE_KEY           =   "Sale_Price" ;
    private static String SALEFORMAT_KEY     =   "Sale_PriceFormat" ;
    private static String CALENDAR_KEY       =   "Calendar" ;
    private static String PURCHASEPRICE_KEY  = "Purchase_Price";
    private static String PURCHASEFORMAT_KEY = "Purchase_PriceFormat";
    private static String KEY = "afsaneh";
    private static int REQUEST_BUTTON = 0;
    private static int SORTLAYOUTREQUEST = 0;
    private static int REQUESTBY = 0 ;
    private static String order = ">=";
    ExcelContent callback;
    int colorsnack;

    Animation Open,Close , openlist , closelist , excelopen , excelclose;



    public static Tab3 newInsatance(String email){
        Tab3 tab3 = new Tab3();
        Bundle bundle = new Bundle();
        bundle.putString("email",email);
        tab3.setArguments(bundle);
        return tab3;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (RecyclerCallBackTab3) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab3,container,false);
        search = view.findViewById(R.id.searchicon);
        objects = new ArrayList<>();
        Email = getArguments().getString("email");
        queue = Volley.newRequestQueue(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        callBack.RecyclerViewCallBackTab3(recyclerView);
        cryptor = new Cryptor();
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        avi = (AVLoadingIndicatorView) view.findViewById(R.id.avi);
        animation = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);
        recycleranim = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
        choose = (AppCompatImageButton) view.findViewById(R.id.choosesearch);
        sortparent = (RelativeLayout) view.findViewById(R.id.sortlayparent);
        Close = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconssearchclose);
        Open = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconsearchopen);
        searchfild = (AppCompatEditText) view.findViewById(R.id.searchfild);
        upper = (CustomCheckBox) view.findViewById(R.id.upper);
        lower = (CustomCheckBox) view.findViewById(R.id.lower);

        closelist = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclosesearch);
        openlist = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopensearch);
        excelopen = AnimationUtils.loadAnimation(getContext(),R.anim.excelanimvisible);
        excelclose = AnimationUtils.loadAnimation(getContext(),R.anim.excelaniminvisible);

        byname =(TextView) view.findViewById(R.id.byname);
        bycelender =(TextView) view.findViewById(R.id.bycelender);
        parentby = (LinearLayoutCompat) view.findViewById(R.id.byparent);
        excel = (AppCompatImageButton) view.findViewById(R.id.exportexcel);
        sort = (AppCompatImageButton) view.findViewById(R.id.sort);

        searchfild.setHint("Search By Name");
        sortparent.setVisibility(View.INVISIBLE);

        excel.setVisibility(View.INVISIBLE);
        excel.startAnimation(excelclose);
        sort.setVisibility(View.INVISIBLE);
        sort.startAnimation(excelclose);

        upper.setChecked(true);
        lower.setChecked(false);


        int themepos = preferences.getInt("theme",0);

        switch (themepos){
            case 0 : color = ContextCompat.getColor(getContext(),R.color.blue);
                     colorsnack = ContextCompat.getColor(getContext(),R.color.snblue);
                     break;
            case 1 : color = ContextCompat.getColor(getContext(),R.color.red);
                     colorsnack = ContextCompat.getColor(getContext(),R.color.snred);
                    break;
            case 2 : color = ContextCompat.getColor(getContext(),R.color.green);
                     colorsnack = ContextCompat.getColor(getContext(),R.color.sngreen);
                    break;
            case 3 : color = ContextCompat.getColor(getContext(),R.color.Tab_background);
                     colorsnack = ContextCompat.getColor(getContext(),R.color.night);
                    break;
        }

        choose.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        if (sort.getVisibility() == View.INVISIBLE){
            REQUESTBY = 0;
            searchfild.setText("");
        }

        excel.setEnabled(false);



        byname.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                    searchfild.setHint("Search By Name");
                    searchfild.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                    REQUEST_BUTTON = 1;
                    OpenClose();
                if (excel.getVisibility() == View.VISIBLE){
                    REQUESTBY = 0 ;
                    excelvisibleOrGone();
                }
                if (sortparent.getVisibility() == View.VISIBLE){
                    SORTLAYOUTREQUEST = 1 ;
                    OpenCloseSortLayout();
                }

            }
        });

        upper.setOnCheckedChangeListener(new CustomCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomCheckBox checkBox, boolean isChecked) {
                lower.setChecked(!isChecked,true);
                SORTLAYOUTREQUEST = 1 ;
                OpenCloseSortLayout();
                SORTLAYOUTREQUEST = 0 ;

            }
        });

        lower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lower.setChecked(upper.isChecked(), true);
                upper.setChecked(!upper.isChecked(), true);
                SORTLAYOUTREQUEST = 1 ;
                OpenCloseSortLayout();
                SORTLAYOUTREQUEST = 0 ;

            }
        });




        bycelender.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                searchfild.setHint("Search By Celendar");
                searchfild.setInputType(InputType.TYPE_CLASS_DATETIME);
                    REQUEST_BUTTON = 1;
                    OpenClose();
                if (excel.getVisibility() == View.INVISIBLE){
                    REQUESTBY = 1 ;
                    excelvisibleOrGone();
                }
            }
        });


        if (REQUEST_BUTTON == 0){
            choose.startAnimation(Close);
            parentby.startAnimation(closelist);
            parentby.setVisibility(View.INVISIBLE);
        }

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenClose();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                avi.setVisibility(View.VISIBLE);
                avi.show();
                objects = new ArrayList<>();
                if (REQUESTBY == 0){
                    search.setEnabled(false);
                    searchmethod(searchfild.getText().toString(),Email);
                }else if (REQUESTBY == 1){
                    if(upper.isChecked()){
                        order = ">=";
                    }else if(lower.isChecked()){
                        order = "<=";
                    }
                    search.setEnabled(false);
                    String calen = searchfild.getText().toString();
                    calen = calen.trim().replace("/","");
                    calen = calen.trim().replace(".","");
                    calen = calen.trim().replace(":","");
                    calen = calen.trim().replace("-","");
                    calen = calen.trim().replace(" ","");

                    final String finalCalen = calen;
                    calendarmethod(calen, new ExcelContent() {
                        @Override
                        public void ExcelObjects(final ArrayList<ContentObject> objects) {
                            if (objects == null){
                                excel.setEnabled(false);
                            }else{
                                excel.setEnabled(true);
                            }
                            excel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    NameAndExport content = new NameAndExport();
                                    content.setName(finalCalen);
                                    content.setObjects(objects);
                                    ExportExcelAsync async = new ExportExcelAsync();
                                    async.execute(content);

                                }
                            });
                        }
                    });

                }
            }
        });
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SORTLAYOUTREQUEST == 0){
                    OpenCloseSortLayout();
                    SORTLAYOUTREQUEST = 1 ;
                }else if (SORTLAYOUTREQUEST == 1){
                    OpenCloseSortLayout();
                    SORTLAYOUTREQUEST = 0 ;
                }
            }
        });


        return view;
    }

    private void OpenClose() {
        if (REQUEST_BUTTON == 0){
            choose.startAnimation(Open);
            REQUEST_BUTTON = 1;
            parentby.startAnimation(openlist);
            parentby.setVisibility(View.VISIBLE);

        }else if(REQUEST_BUTTON == 1){
            choose.startAnimation(Close);
            REQUEST_BUTTON = 0;
            parentby.startAnimation(closelist);
            parentby.setVisibility(View.INVISIBLE);
        }
    }

    private void OpenCloseSortLayout() {
        if (SORTLAYOUTREQUEST == 0){
            SORTLAYOUTREQUEST= 1;
            sortparent.startAnimation(openlist);
            sortparent.setVisibility(View.VISIBLE);

        }else if(SORTLAYOUTREQUEST == 1){
            SORTLAYOUTREQUEST = 0;
            sortparent.startAnimation(closelist);
            sortparent.setVisibility(View.INVISIBLE);
        }
    }

    private void excelvisibleOrGone(){
        if (REQUESTBY == 0 ){
                excel.setVisibility(View.INVISIBLE);
                excel.startAnimation(excelclose);
                sort.setVisibility(View.INVISIBLE);
                sort.startAnimation(excelclose);
        }else if (REQUESTBY == 1 ){
                excel.startAnimation(excelopen);
                excel.setVisibility(View.VISIBLE);
                sort.startAnimation(excelopen);
                sort.setVisibility(View.VISIBLE);
        }
    }

    private void searchmethod(final String Name, final String email) {
        String uri = "http://www.inventory-customer.com/source/Search.php";
        request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length() ; i++) {
                        ContentObject object = new ContentObject();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString(NAME_KEY);
                        String stock = cryptor.Decrypt(jsonObject.getString(STOCK_KEY),KEY);
                        String barcode = cryptor.Decrypt(jsonObject.getString(BARCODE_KEY),KEY);
                        String encasement = cryptor.Decrypt(jsonObject.getString(ENCASEMENT_KEY),KEY);
                        String stockformat = cryptor.Decrypt(jsonObject.getString(STOCKFORMAT_KEY),KEY);
                        String sale = cryptor.Decrypt(jsonObject.getString(SALE_KEY),KEY);
                        String saleformat = cryptor.Decrypt(jsonObject.getString(SALEFORMAT_KEY),KEY);

                        object.setID(jsonObject.getInt(ID_KEY));
                        object.setName(name);
                        object.setStockFormat(stockformat);
                        object.setStock(stock);
                        object.setSaleFormat(saleformat);
                        object.setSale_Price(sale);
                        object.setBarCode(barcode);
                        object.setEncasement(encasement);
                        object.setImageAddress(jsonObject.getString(IMAGENAMR_KEY));
                        objects.add(object);
                        search.setEnabled(true);
                    }
                    manager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    adapter = new MyAdapter(getContext(),objects,email);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setAnimation(recycleranim);
                    search.setEnabled(true);


                } catch (JSONException e) {
                    e.printStackTrace();

                }
                avi.setAnimation(animation);
                avi.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                search.setEnabled(true);

                request.setShouldCache(false);
                request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(request);

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("email",Email);
                map.put("name",Name);
                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    private void calendarmethod(final String calendar, final ExcelContent callback) {
        String uri = "http://www.inventory-customer.com/source/Calendarget.php";


        calendarrequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length() ; i++) {
                        ContentObject object = new ContentObject();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString(NAME_KEY);
                        String stock = cryptor.Decrypt(jsonObject.getString(STOCK_KEY),KEY);
                        String barcode = cryptor.Decrypt(jsonObject.getString(BARCODE_KEY),KEY);
                        String stockformat = cryptor.Decrypt(jsonObject.getString(STOCKFORMAT_KEY),KEY);
                        String sale = cryptor.Decrypt(jsonObject.getString(SALE_KEY),KEY);
                        String saleformat = cryptor.Decrypt(jsonObject.getString(SALEFORMAT_KEY),KEY);
                        String calender = jsonObject.getString(CALENDAR_KEY);
                        String purcahseprice = cryptor.Decrypt(jsonObject.getString(PURCHASEPRICE_KEY),KEY);
                        String purchaseformat = cryptor.Decrypt(jsonObject.getString(PURCHASEFORMAT_KEY),KEY);

                        StringBuilder builder = new StringBuilder(calender);
                        builder.insert(4,'/');
                        builder.insert(7,'/');
                        String calendarchange = builder.toString();


                        object.setID(jsonObject.getInt(ID_KEY));
                        object.setName(name);
                        object.setStockFormat(stockformat);
                        object.setStock(stock);
                        object.setSaleFormat(saleformat);
                        object.setSale_Price(sale);
                        object.setPurchase_Price(purcahseprice);
                        object.setPurchaseFomat(purchaseformat);
                        object.setCalende(calendarchange);
                        object.setBarCode(barcode);
                        object.setCalende(jsonObject.getString(CALENDAR_KEY));
                        objects.add(object);
                    }
                    manager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    sortAdapter = new SortAdapter(objects,getContext());
                    recyclerView.setAdapter(sortAdapter);
                    recyclerView.setAnimation(recycleranim);
                    callback.ExcelObjects(objects);
                    search.setEnabled(true);



                } catch (JSONException e) {
                    search.setEnabled(true);
                }
                avi.setAnimation(animation);
                avi.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Check Your Internet", Toast.LENGTH_SHORT).show();
                avi.setAnimation(animation);
                avi.hide();
                search.setEnabled(true);

                calendarrequest.setShouldCache(false);
                calendarrequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(calendarrequest);


            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("email",Email);
                map.put("calendar",calendar);
                map.put("order",order);
                return map;
            }
        };

        calendarrequest.setShouldCache(false);
        calendarrequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(calendarrequest);

    }

    @Override
    public void onResume() {
        super.onResume();
        boolean tab2 = preferences.getBoolean("checktab2",true);
        if (tab2){
            editor.putBoolean("tab2",false).apply();
        }else if (!tab2){
            editor.remove("checktab2").apply();
        }
    }
    private class NameAndExport{
        ArrayList<ContentObject> objects;
        String Name;

        public void setName(String name) {
            Name = name;
        }

        public void setObjects(ArrayList<ContentObject> objects) {
            this.objects = objects;
        }

        public ArrayList<ContentObject> getObjects() {
            return objects;
        }

        public String getName() {
            return Name;
        }
    }
    @Override
    public void onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) { }

    private class ExportExcelAsync extends AsyncTask<NameAndExport,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tSnackbar = TSnackbar.make(getActivity().findViewById(android.R.id.content),"Working on it...", TSnackbar.LENGTH_SHORT);
            View view = tSnackbar.getView();
            view.setBackgroundColor(Color.parseColor("#dedede"));
            TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
            textView.setTextColor(colorsnack);
            tSnackbar.show();
        }

        @Override
        protected String doInBackground(NameAndExport... arrayLists) {
            NameAndExport content = arrayLists[0];
            ArrayList<ContentObject> objects = content.getObjects();
            String name = content.getName();

            String filename = name+".xls";
            File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/Inventory/Excel/",filename);

            WritableWorkbook workbook = null;
            WorkbookSettings workbookSettings = new WorkbookSettings();
            workbookSettings.setLocale(new Locale("en","EN"));

            try {
                workbook = Workbook.createWorkbook(file,workbookSettings);
                uploadfiles(workbook,objects,name);
                workbook.write();
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tSnackbar = TSnackbar.make(getActivity().findViewById(android.R.id.content),"Done", TSnackbar.LENGTH_SHORT);
            View view = tSnackbar.getView();
            view.setBackgroundColor(Color.parseColor("#dedede"));
            TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
            textView.setTextColor(colorsnack);
            tSnackbar.show();
        }
    }

    private void uploadfiles(WritableWorkbook workbook, ArrayList<ContentObject> objects ,String name) {
        WritableSheet sheet = workbook.createSheet(name,0);
        try {
            sheet.addCell(new Label(0,0,"Name"));
            sheet.addCell(new Label(1,0,"Stock"));
            sheet.addCell(new Label(2,0,"Prichase Price"));
            sheet.addCell(new Label(3,0,"Sale Price"));
            sheet.addCell(new Label(4,0,"BarCode"));
            sheet.addCell(new Label(5,0,"calendar"));
            StringBuilder builder;

            for (int i = 0; i < objects.size() ; i++) {
                sheet.addCell(new Label(0,i+1,objects.get(i).getName()));
                sheet.addCell(new Label(1,i+1,objects.get(i).getStock()+"("+objects.get(i).getStockFormat()+")"));
                sheet.addCell(new Label(2,i+1,objects.get(i).getPurchase_Price()+"("+objects.get(i).getPurchaseFomat()+")"));
                sheet.addCell(new Label(3,i+1,objects.get(i).getSale_Price()+"("+objects.get(i).getSaleFormat()+")"));
                sheet.addCell(new Label(4,i+1,objects.get(i).getBarCode()));
                builder = new StringBuilder(objects.get(i).getCalende());
                builder.insert(4,'/');
                builder.insert(7,'/');
                sheet.addCell(new Label(5,i+1,builder.toString()));

            }
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }


}
interface ExcelContent{
    void ExcelObjects(ArrayList<ContentObject> objects);
}
