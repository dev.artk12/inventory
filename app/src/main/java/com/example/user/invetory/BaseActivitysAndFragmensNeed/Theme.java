package com.example.user.invetory.BaseActivitysAndFragmensNeed;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.example.user.invetory.R;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.transformer.UltraDepthScaleTransformer;

public class Theme extends Fragment {

    UltraViewPager ultraViewPager;
    PagerAdapter adapter;
    UltraViewPager.Orientation gravity_indicator;
    int[] themes = {R.drawable.blue, R.drawable.red, R.drawable.green};
    ImageButton ok;
    SharedPreferences preferences;
    MyInterface myInterface;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            myInterface = (MyInterface) context;
        } catch (ClassCastException e) {
            //throw new ClassCastException(activity.toString() + " must implement MyInterface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        myInterface.lockDrawer();

        View view = inflater.inflate(R.layout.activity_theme,container,false);

        try {
            preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            final SharedPreferences.Editor editor = preferences.edit();
            ok = (ImageButton) view.findViewById(R.id.OK);
            ultraViewPager = (UltraViewPager) view.findViewById(R.id.viewpager);
            adapter = new CustomThemePagerAdapter(themes);
            ultraViewPager.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
            ultraViewPager.setAdapter(adapter);
            //ultraViewPager.setMultiScreen(0.96f);
            ultraViewPager.setInfiniteLoop(true);
            //ultraViewPager.setAutoMeasureHeight(false);
            ultraViewPager.setPageTransformer(false, new UltraDepthScaleTransformer());
            ultraViewPager.initIndicator();
            ultraViewPager.getIndicator().setOrientation(gravity_indicator);
            ultraViewPager.getIndicator().setFocusResId(0).setNormalResId(0);
            ultraViewPager.getIndicator().setFocusColor(Color.BLUE).setNormalColor(Color.WHITE)
                    .setRadius((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics()));
            int gravity_ver = Gravity.CENTER_HORIZONTAL ;
            ultraViewPager.getIndicator().setGravity(gravity_ver | Gravity.BOTTOM);
            ultraViewPager.getIndicator().build();

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int possion = ultraViewPager.getCurrentItem();
                    editor.remove("theme");
                    editor.putInt("theme" , possion).apply();
                    Intent intent = new Intent(getActivity(),Entrance.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            });

        }catch (OutOfMemoryError error){
            Intent intent = new Intent(getActivity(),Entrance.class);
            startActivity(intent);
            getActivity().finish();

        }


        return view;
    }
    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public void onDestroyView() {
        myInterface.unlockDrawer();
        super.onDestroyView();
    }
}