package com.example.user.invetory.BaseActivitysAndFragmensNeed;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.example.user.invetory.MyAdapter;


public class TochHelper extends ItemTouchHelper.SimpleCallback {

    TochHelperListener listener;
    GestureDetector mGestureDetector;
    Context context;


    public TochHelper(int dragDirs, int swipeDirs , TochHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    public TochHelper(int dragDirs, int swipeDirs ,Context context,TochHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;

        this.context = context;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }



    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        if (listener != null){
            listener.onMove(recyclerView,viewHolder,target);
        }
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
    }



    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

         if (actionState == ItemTouchHelper.ACTION_STATE_DRAG){
            super.onChildDraw(c,recyclerView,viewHolder,dX,dY,actionState,isCurrentlyActive);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

         if (actionState == ItemTouchHelper.ACTION_STATE_DRAG){
            super.onChildDrawOver(c,recyclerView,viewHolder,dX,dY,actionState,isCurrentlyActive);
        }
    }
}
