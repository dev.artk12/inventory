package com.example.user.invetory.CreateObject;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.LocalHelper;
import com.example.user.invetory.MyContextWrapper;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;
import com.example.user.invetory.Ucrop;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.yalantis.ucrop.UCrop;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AddObject extends AppCompatActivity implements BarCodeFace,ImageFace, MainCallBack,AddObjectCallBack {


    private static final int REQUEST_PERMISSION = 101 ;
    VerticalViewPager viewPager;
    Adapter adapter;
    Uri uri;
    String barcode;
    boolean bool;
    Output_files files;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Map<String,String> content;
    Map<String,View> viewMap;
    private static final int REQUEST_IMAGE = 1 ;
    String [] permissions = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};
    Button save ;
    MainCallBack mainCallBack;
    Cryptor cryptor;
    private static String KEY = "afsaneh";
    SweetAlertDialog dialog;
    RequestQueue queue;
    AlertDialog.Builder vollydialog;
    StringRequest request;





    private static String IMAGE_KEY = "IMAGE";
    private static String NAME_KEY = "NAME";
    private static String STOCK_KEY = "STOCK";
    private static String CREATOR_KEY = "CREATOR";
    private static String MODEL_KEY = "MODEL";
    private static String GROUP_KEY = "GROUP";
    private static String DESCRIPTION_KEY = "DESCRIPTION";
    private static String PURCHASERICE_KEY = "PARCHASEPRICE";
    private static String SALEPRICE_KEY = "SALEPRICE";
    private static String ENCASEMENT_KEY = "ENCASEMENT";
    private static String BARCODE_KEY = "BARCODE";
    private static String STOCKFORMAT_KEY = "STOCKFORMAT";
    private static String SALEFORMAT_KEY = "SALEFORMAT";
    private static String PURCHASEFORMAT_KEY = "PURCHASEFORMAT";
    int color;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(AddObject.this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme", 0);
        dialog = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        String language = preferences.getString("language","en");

        switch (themepos) {
            case 0:
                setTheme(R.style.DefultTheme);
                color = ContextCompat.getColor(this,R.color.snblue);
                dialog.getProgressHelper().setBarColor(Color.parseColor("#0c7f99"));
                break;
            case 1:
                setTheme(R.style.RedTheme);
                color = ContextCompat.getColor(this,R.color.snred);
                dialog.getProgressHelper().setBarColor(Color.parseColor("#990c39"));
                break;
            case 2:
                setTheme(R.style.GreenTheme);
                color = ContextCompat.getColor(this,R.color.sngreen);
                dialog.getProgressHelper().setBarColor(Color.parseColor("#065029"));
                break;
            case 3:
                setTheme(R.style.NightMode);
                color = ContextCompat.getColor(this,R.color.night);
                dialog.getProgressHelper().setBarColor(Color.parseColor("#a2090b94"));
                break;
        }

        setContentView(R.layout.addobjectactivity);
        viewPager = (VerticalViewPager) findViewById(R.id.viewpager);
        files = new Output_files(this);
        viewMap = new HashMap<>();
        content = new HashMap<>();

        vollydialog = new AlertDialog.Builder(this);






        dialog.setTitleText("Saving...");
        dialog.setCancelable(false);


        handleSSLHandshake();


        save = (Button) findViewById(R.id.save);
        save.setVisibility(View.INVISIBLE);
        mainCallBack = (MainCallBack) this;
        cryptor = new Cryptor();
        queue = Volley.newRequestQueue(this);

        adapter = new Adapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mainCallBack.position(positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0 : save.setVisibility(View.INVISIBLE); break;
                    case 1 : save.setVisibility(View.VISIBLE); break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = files.read("email");
                editor.putBoolean("tab2",true);
                editor.putBoolean("tab1",true);
                editor.putBoolean("checktab2",false);

                editor.apply();

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyyyyHHmmss", Locale.ENGLISH);
                String timestamp = simpleDateFormat.format(new Date());


                String image = content.get(IMAGE_KEY);
                String name = content.get(NAME_KEY);
                String stock = content.get(STOCK_KEY);
                String creator = content.get(CREATOR_KEY);
                String model = content.get(MODEL_KEY);
                String group = content.get(GROUP_KEY);
                String description = content.get(DESCRIPTION_KEY);
                String purchaseprice = content.get(PURCHASERICE_KEY);
                String saleprice = content.get(SALEPRICE_KEY);
                String encasement = content.get(ENCASEMENT_KEY);
                String barcode = content.get(BARCODE_KEY);
                String stockformat = content.get(STOCKFORMAT_KEY);
                String saleformat = content.get(SALEFORMAT_KEY);
                String purchaseformat = content.get(PURCHASEFORMAT_KEY);
                String imagename = barcode+timestamp+".png";


                String stockEncryption = cryptor.Encrypt(stock,KEY);
                String creatorEncryption = cryptor.Encrypt(creator,KEY);
                String modelkEncryption = cryptor.Encrypt(model,KEY);
                String descriptionEncryption = cryptor.Encrypt(description,KEY);
                String purchasepriceEncryption = cryptor.Encrypt(purchaseprice,KEY);
                String salepriceEncryption = cryptor.Encrypt(saleprice,KEY);
                String encasementEncryption = cryptor.Encrypt(encasement,KEY);
                String barcodeEncryption = cryptor.Encrypt(barcode,KEY);
                String stockformatEncryption = cryptor.Encrypt(stockformat,KEY);
                String saleformatEncryption = cryptor.Encrypt(saleformat,KEY);
                String purchaseformatEncryption = cryptor.Encrypt(purchaseformat,KEY);


                if (image != null) {

                    boolean check = checkfild(stockformat,saleformat,purchaseformat,image,name,stock,creator,model,group,description,purchaseprice,saleprice,encasement,barcode);

                    if (check){
                        try {
                            requestvalue(stockformatEncryption,saleformatEncryption,purchaseformatEncryption,email,name,stockEncryption,creatorEncryption,modelkEncryption,group,
                                    purchasepriceEncryption,salepriceEncryption,encasementEncryption,image,descriptionEncryption,barcodeEncryption,imagename);
                            dialog.show();
                        }catch (OutOfMemoryError o){
                            Toast.makeText(AddObject.this, o.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            Toast.makeText(AddObject.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(AddObject.this, "Please Fill All Fields", Toast.LENGTH_SHORT).show();
                    }

                } else
                    Toast.makeText(AddObject.this, "Please Fill All Fields", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void BarcodeEditText(EditText barcode) {
        viewMap.put("edt",barcode);
    }


    @Override
    public void Imagefcerequest(final ImageView img) {
        viewMap.put("img",img);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean camrabool = checkPermissionCamera();
                boolean ExternalStorage = checkPermissionExternalStorage();

                if (camrabool && ExternalStorage){
                    CameraRequest();
                }else {
                    ActivityCompat.requestPermissions(AddObject.this,permissions,REQUEST_PERMISSION);
                }
            }
        });

    }

    @Override
    public void position(float pos) {
        if (save.getVisibility() == View.VISIBLE && pos > 0){
            save.setAlpha(pos);
        }
    }

    @Override
    public void Tab1_Result(String stockformat,String Name, String Stock, String Creator, String Model, String Group) {


            content.put(NAME_KEY,Name);
            if (Stock.length() >0){
                Double stockdouble = Double.valueOf(Stock);
                int stockstring = stockdouble.intValue();
                content.put(STOCK_KEY,String.valueOf(stockstring));
            }
            content.put(CREATOR_KEY,Creator);
            content.put(MODEL_KEY,Model);
            content.put(GROUP_KEY,Group);
            content.put(STOCKFORMAT_KEY,stockformat);


    }

    @Override
    public void Tab2_Result(String saleformat , String purchaseformat,String Description, String PurchasePrice, String SalePrice, String encasement, String BarCode) {
        content.put(DESCRIPTION_KEY,Description);
        content.put(PURCHASERICE_KEY,PurchasePrice);
        content.put(SALEPRICE_KEY,SalePrice);
        content.put(ENCASEMENT_KEY,encasement);
        content.put(BARCODE_KEY,BarCode);
        content.put(SALEFORMAT_KEY,saleformat);
        content.put(PURCHASEFORMAT_KEY,purchaseformat);
    }

    public class Adapter extends FragmentPagerAdapter {


        public Adapter (FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 : return new AddObjectTab1();
                case 1 : return new AddObjectTab2();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)  {

        Ucrop ucrop = new Ucrop(color);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            ucrop.startCropActivity(AddObject.this,uri,"object");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Bitmap bitmap = Ucrop.handleCropResult(AddObject.this,data);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            byte[] bytes = stream.toByteArray();
            String content = Base64.encodeToString(bytes,Base64.DEFAULT);
            this.content.put(IMAGE_KEY,content);

            ImageView imageView = (ImageView) viewMap.get("img");
            imageView.setImageBitmap(bitmap);
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        IntentResult Result = IntentIntegrator.parseActivityResult(requestCode , resultCode ,data);
        if(Result != null){
            if(Result.getContents() == null){
                Log.d("MainActivity" , "cancelled scan");
            }
            else {
                EditText editText = (EditText) viewMap.get("edt");
                editText.setText(Result.getContents());
            }
        }
        else {
            super.onActivityResult(requestCode , resultCode , data);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            CameraRequest();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void CameraRequest() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/inventory/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        startActivityForResult(CamIntent,REQUEST_IMAGE);
    }

    boolean checkPermissionCamera(){
        int res = checkCallingOrSelfPermission(permissions[0]);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    boolean checkPermissionExternalStorage(){
        int res = checkCallingOrSelfPermission(permissions[1]);
        return (res == PackageManager.PERMISSION_GRANTED);
    }


    private boolean checkfild(String stockformat,String saleformat , String purchaseformat,String image, String name, String stock, String creator, String model,
                              String group, String description, String purchaseprice, String saleprice, String encasement, String barcode) {


        if (!stockformat.isEmpty()&&!saleformat.isEmpty()&&!purchaseformat.isEmpty()
                &&!name.isEmpty()&& !stock.isEmpty() && !creator.isEmpty()&& !model.isEmpty()&& !group.isEmpty()&& !image.equals("") && image.length() > 100 &&
                !description.isEmpty()&& !purchaseprice.isEmpty()&& !saleprice.isEmpty()&& !encasement.isEmpty()&& !barcode.isEmpty()) {

                return true;
        }else
            return false;
    }



    public void requestvalue(final String stockformat, final String saleformat , final String purchaseformat, final String email, final String name , final String stock
            , final String creator , final String model , final String group , final String Purchase_price,
                             final String Sale_price, final String encasement, final String photo, final String des, final String barcode, final String imagename){

        String uri = "http://www.inventory-customer.com/source/writevalues.php";

        request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismissWithAnimation();
                finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismissWithAnimation();
                vollydialog.setMessage("Please Check Your Network").setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.setShouldCache(false);
                        request.setRetryPolicy(new DefaultRetryPolicy(
                                (int) TimeUnit.SECONDS.toMillis(2),
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queue.add(request);
                    }
                }).setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }).setCancelable(false).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> getparams = new HashMap<>();

                getparams.put("email",email);
                getparams.put("name",name);
                getparams.put("stock",stock);
                getparams.put("creator",creator);
                getparams.put("model",model);
                getparams.put("group",group);
                getparams.put("p_price",Purchase_price);
                getparams.put("s_price",Sale_price);
                getparams.put("encasement",encasement);
                getparams.put("image",photo);
                getparams.put("imagename",imagename);
                getparams.put("des",des);
                getparams.put("barcode",barcode);
                getparams.put("stockformat",stockformat);
                getparams.put("saleformat",saleformat);
                getparams.put("purchaseformat",purchaseformat);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                Date date = new Date();
                StringBuilder calenderformat = new StringBuilder(simpleDateFormat.format(date));
                getparams.put("calender",calenderformat.toString());

                return getparams;
            }
        };
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(2),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

interface MainCallBack {
    void position(float pos);
    //void BitmapCallBack(Bitmap bitmap);
}