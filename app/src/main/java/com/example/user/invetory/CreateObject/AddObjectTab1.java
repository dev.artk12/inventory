package com.example.user.invetory.CreateObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.example.user.invetory.R;
import com.example.user.invetory.spinneradapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AddObjectTab1 extends Fragment implements TextWatcher {


    ImageView img;
    Uri uri;
    ImageFace face;
    DisplayMetrics metrics;
    AppCompatEditText name,stock,creator,model,group , format;
    AppCompatTextView calender;
    ImageButton next;
    LinearLayout parent;
    AddObjectCallBack callBack;
    RelativeLayout stockparent;
    int colorwrong;

    RecyclerView spinneritem;
    AppCompatImageButton imageButton;
    private static int TAG = 0 ;
    ArrayList<String> names;
    Animation listanim, iconanim , fadein , fadeout;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        face = (ImageFace) context;
        callBack = (AddObjectCallBack) context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.addobject1,container,false);
        img = (ImageView) view.findViewById(R.id.img);
        name = (AppCompatEditText) view.findViewById(R.id.name);
        stock = (AppCompatEditText) view.findViewById(R.id.stock);
        creator = (AppCompatEditText) view.findViewById(R.id.creator);
        model = (AppCompatEditText) view.findViewById(R.id.model);
        group = (AppCompatEditText) view.findViewById(R.id.group);
        next = (ImageButton) view.findViewById(R.id.next);
        parent = (LinearLayout) view.findViewById(R.id.parent);
        spinneritem = (RecyclerView) view.findViewById(R.id.listview);
        format = (AppCompatEditText) view.findViewById(R.id.format);
        imageButton = (AppCompatImageButton) view.findViewById(R.id.dropdown);
        calender = (AppCompatTextView) view.findViewById(R.id.calender);
        stockparent = (RelativeLayout) view.findViewById(R.id.stockparent);
        fadein = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
        fadeout = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);
        colorwrong = ContextCompat.getColor(getContext(),R.color.wrongcheckedtsignup);


        format.setEnabled(false);
        metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        imageButton.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        StringBuilder calenderformat = new StringBuilder(simpleDateFormat.format(date));
        calenderformat.insert(4,"/");
        calenderformat.insert(7,"/");

        calender.setText(calenderformat.toString());


        names = new ArrayList<>();
        names.add("hel");
        names.add("fuc");
        names.add("cd");
        names.add("ddd");

        spinneradapter arrayAdapter = new spinneradapter(false,getContext(),format, names);
        spinneritem.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        spinneritem.setAdapter(arrayAdapter);


        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TAG == 0){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopen);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerlist);
                    spinneritem.setVisibility(View.VISIBLE);
                    spinneritem.startAnimation(listanim);
                    spinneritem.setEnabled(true);
                    creator.setVisibility(View.INVISIBLE);
                    creator.startAnimation(fadeout);
                    imageButton.startAnimation(iconanim);
                    format.setEnabled(false);
                    hideKeyboard(getActivity());
                    checkEdittext(false);
                    TAG = 1;
                }else if (TAG == 1){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclose);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinneredt);
                    spinneritem.setVisibility(View.INVISIBLE);
                    spinneritem.setEnabled(false);
                    spinneritem.startAnimation(listanim);
                    imageButton.startAnimation(iconanim);
                    creator.setVisibility(View.VISIBLE);
                    creator.startAnimation(fadein);
                    format.setEnabled(true);
                    format.requestFocus();
                    TAG = 2;
                    checkEdittext(false);
                }else if (TAG == 2){
                    creator.setEnabled(true);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerclose);
                    imageButton.startAnimation(iconanim);
                    spinneritem.setEnabled(false);
                    format.setEnabled(false);
                    TAG = 0;
                    checkEdittext(true);
                }
            }
        });



        callBack.Tab1_Result(format.getText().toString(),name.getText().toString(),stock.getText().toString(),creator.getText().toString(),model.getText().toString(),group.getText().toString());

        setup_first(metrics,name,stock,creator,model,group,parent,img,next);

        changelistenr(format,name,stock,creator,model,group);

        face.Imagefcerequest(img);

        return view;
    }

    private void checkEdittext(boolean b) {
        name.setEnabled(b);
        stock.setEnabled(b);
        creator.setEnabled(b);
        model.setEnabled(b);
        group.setEnabled(b);

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    private static void setup_first(DisplayMetrics metrics,EditText name,
                             EditText stock, EditText creator, EditText model, EditText group,
                                    LinearLayout parent, ImageView img, ImageButton next) {
        img.getLayoutParams().height = metrics.heightPixels/ 6;
        img.getLayoutParams().width = metrics.widthPixels/ 3;

        LinearLayout.LayoutParams imgparams = (LinearLayout.LayoutParams) img.getLayoutParams();
        imgparams.setMargins(0,metrics.heightPixels/10,0,0);
        name.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        stock.getLayoutParams().width = (int) (metrics.widthPixels / 2);
        creator.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        model.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        group.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);

        img.getLayoutParams().height = metrics.heightPixels/ 6;
        img.getLayoutParams().width = metrics.widthPixels/ 3;



        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) parent.getLayoutParams();
        params.setMargins(0,5,0,0);
        parent.setLayoutParams(params);


        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) stock.getLayoutParams();
        params2.setMargins(0,metrics.heightPixels/40,0,0);
        stock.setLayoutParams(params2);


        RelativeLayout.LayoutParams params3 = (RelativeLayout.LayoutParams) creator.getLayoutParams();
        params3.setMargins(0,metrics.heightPixels/40,0,0);
        creator.setLayoutParams(params3);


        LinearLayout.LayoutParams params4 = (LinearLayout.LayoutParams) model.getLayoutParams();
        params4.setMargins(0,metrics.heightPixels/40,0,0);
        model.setLayoutParams(params4);


        LinearLayout.LayoutParams params5 = (LinearLayout.LayoutParams) group.getLayoutParams();
        params5.setMargins(0,metrics.heightPixels/40,0,0);
        group.setLayoutParams(params5);

        LinearLayout.LayoutParams nextLayoutParams = (LinearLayout.LayoutParams) next.getLayoutParams();
        nextLayoutParams.setMargins(0,metrics.heightPixels/40,0,0);
        next.setLayoutParams(nextLayoutParams);
    }
    private void changelistenr(EditText stockformat,EditText name, EditText stock, EditText creator, EditText model, EditText group){
        name.addTextChangedListener(this);
        stock.addTextChangedListener(this);
        creator.addTextChangedListener(this);
        model.addTextChangedListener(this);
        group.addTextChangedListener(this);
        stockformat.addTextChangedListener(this);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable s) {
        callBack.Tab1_Result(format.getText().toString(),name.getText().toString(),stock.getText().toString(),creator.getText().toString(),model.getText().toString(),group.getText().toString());
    }


}
