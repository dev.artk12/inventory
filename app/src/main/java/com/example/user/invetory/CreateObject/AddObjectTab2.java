package com.example.user.invetory.CreateObject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.user.invetory.R;
import com.example.user.invetory.spinneradapter;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;


public class AddObjectTab2 extends Fragment implements TextWatcher {


    AppCompatEditText barcode,description,purchase_price,sale_price,encasement , format1 , format2;
    BarCodeFace face;
    ImageView scanner;
    DisplayMetrics metrics;
    LinearLayout parent;
    RelativeLayout barcodeparent;
    AddObjectCallBack callBack;

    RecyclerView spinneritem1 , spinneritem2;
    AppCompatImageButton imageButton1 , imageButton2;
    private static int TAG_ONE = 0 ;
    private static int TAG_TWO = 0 ;

    ArrayList<String> format;
    Animation listanim, iconanim , fadein , fadeout;

    public AddObjectTab2() { }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        face = (BarCodeFace) context;
        callBack = (AddObject) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.addobject2,container,false);
        scanner = (ImageView) view.findViewById(R.id.barcod_scanner);
        barcode = (AppCompatEditText)view.findViewById(R.id.barcod);
        description = (AppCompatEditText) view.findViewById(R.id.description);
        purchase_price = (AppCompatEditText) view.findViewById(R.id.p_price);
        sale_price = (AppCompatEditText) view.findViewById(R.id.s_price);
        encasement = (AppCompatEditText) view.findViewById(R.id.encasement);
        barcodeparent = (RelativeLayout) view.findViewById(R.id.barcode_parent);
        parent = (LinearLayout) view.findViewById(R.id.parent2);
        spinneritem1 = (RecyclerView) view.findViewById(R.id.listview);
        spinneritem2 = (RecyclerView) view.findViewById(R.id.listviewtwo);
        format1 = (AppCompatEditText) view.findViewById(R.id.format);
        imageButton1 = (AppCompatImageButton) view.findViewById(R.id.dropdown);
        format2 = (AppCompatEditText) view.findViewById(R.id.formattwo);
        imageButton2 = (AppCompatImageButton) view.findViewById(R.id.dropdowntwo);
        fadein = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
        fadeout = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);
        face.BarcodeEditText(barcode);
        metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        imageButton1.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        imageButton2.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);


        format1.setEnabled(false);
        format2.setEnabled(false);

        format = new ArrayList<>();
        format.add("hel");
        format.add("fuc");
        format.add("cd");
        format.add("ddd");

        spinneradapter arrayAdapter = new spinneradapter(false,getContext(),format1, format);
        spinneritem1.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        spinneritem1.setAdapter(arrayAdapter);

        spinneradapter arrayAdapter2 = new spinneradapter(false,getContext(),format2, format);
        spinneritem2.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        spinneritem2.setAdapter(arrayAdapter2);



        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TAG_ONE == 0){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopen);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerlist);
                    spinneritem1.setVisibility(View.VISIBLE);
                    spinneritem1.startAnimation(listanim);
                    spinneritem1.setEnabled(true);
                    sale_price.setVisibility(View.INVISIBLE);
                    sale_price.startAnimation(fadeout);
                    imageButton2.setVisibility(View.INVISIBLE);
                    imageButton2.startAnimation(fadeout);
                    imageButton1.startAnimation(iconanim);
                    format1.setEnabled(false);
                    checkbuttons(false);
                    TAG_ONE = 1;
                }else if (TAG_ONE == 1){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclose);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinneredt);
                    spinneritem1.setVisibility(View.INVISIBLE);
                    spinneritem1.setEnabled(false);
                    spinneritem1.startAnimation(listanim);
                    imageButton1.startAnimation(iconanim);
                    sale_price.setVisibility(View.VISIBLE);
                    sale_price.startAnimation(fadein);
                    imageButton2.setVisibility(View.VISIBLE);
                    imageButton2.startAnimation(fadein);
                    format1.setEnabled(true);
                    format1.requestFocus();
                    TAG_ONE = 2;
                    checkbuttons(false);
                }else if (TAG_ONE == 2){
                    sale_price.setEnabled(true);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerclose);
                    imageButton1.startAnimation(iconanim);
                    spinneritem1.setEnabled(false);
                    format1.setEnabled(false);
                    TAG_ONE = 0;
                    checkbuttons(true);
                }
            }
        });



        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TAG_TWO == 0){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopen);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerlist);
                    spinneritem2.setVisibility(View.VISIBLE);
                    spinneritem2.startAnimation(listanim);
                    spinneritem2.setEnabled(true);
                    encasement.setVisibility(View.INVISIBLE);
                    encasement.startAnimation(fadeout);
                    imageButton2.startAnimation(iconanim);
                    format2.setEnabled(false);
                    TAG_TWO = 1;
                    checkbuttonsTwo(false);

                }else if (TAG_TWO == 1){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclose);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinneredt);
                    spinneritem2.setVisibility(View.INVISIBLE);
                    spinneritem2.setEnabled(false);
                    spinneritem2.startAnimation(listanim);
                    imageButton2.startAnimation(iconanim);
                    encasement.setVisibility(View.VISIBLE);
                    encasement.startAnimation(fadein);
                    format2.setEnabled(true);
                    format2.requestFocus();
                    TAG_TWO = 2;
                    checkbuttonsTwo(false);

                }else if (TAG_TWO == 2){
                    encasement.setEnabled(true);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerclose);
                    imageButton2.startAnimation(iconanim);
                    spinneritem2.setEnabled(false);
                    format2.setEnabled(false);
                    TAG_TWO = 0;
                    checkbuttonsTwo(true);

                }
            }
        });








        setup_secend(metrics,parent,description,purchase_price,sale_price,encasement,barcodeparent,barcode);

        callBack.Tab2_Result(format2.getText().toString(),format1.getText().toString(),description.getText().toString(),purchase_price.getText().toString(),
                sale_price.getText().toString(),encasement.getText().toString(),barcode.getText().toString());

        changelistener(format2,format1,description,purchase_price,sale_price,encasement,barcode);


        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());
                intentIntegrator.setDesiredBarcodeFormats(intentIntegrator.ALL_CODE_TYPES);
                intentIntegrator.setBeepEnabled(false);
                intentIntegrator.setCameraId(0);
                intentIntegrator.setPrompt("SCAN");
                intentIntegrator.setBarcodeImageEnabled(false);
                intentIntegrator.setOrientationLocked(false);
                intentIntegrator.initiateScan();
            }
        });

        return view;
    }

    private void checkbuttonsTwo(boolean b) {

        description.setEnabled(b);
        purchase_price.setEnabled(b);
        sale_price.setEnabled(b);
        encasement.setEnabled(b);
        barcode.setEnabled(b);
        imageButton1.setEnabled(b);
    }

    private void checkbuttons(boolean b) {
        description.setEnabled(b);
        purchase_price.setEnabled(b);
        sale_price.setEnabled(b);
        encasement.setEnabled(b);
        barcode.setEnabled(b);
        imageButton2.setEnabled(b);
    }

    private void setup_secend(DisplayMetrics metrics, LinearLayout parent2,
                              AppCompatEditText description, AppCompatEditText p_price, AppCompatEditText s_price,
                              AppCompatEditText encasement , RelativeLayout barcodeparent,AppCompatEditText barcode) {

        description.getLayoutParams().width = (int) (metrics.widthPixels / 1.2);
        description.getLayoutParams().height = (int) (metrics.heightPixels / 6);
        LinearLayout.LayoutParams decparams = (LinearLayout.LayoutParams) description.getLayoutParams();
        decparams.setMargins(0,metrics.heightPixels/10,0,0);
        description.setLayoutParams(decparams);
        p_price.getLayoutParams().width = (int) (metrics.widthPixels / 2);
        s_price.getLayoutParams().width = (int) (metrics.widthPixels / 2);
        encasement.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        barcode.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        barcodeparent.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);


        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) parent2.getLayoutParams();
        params.setMargins(0,metrics.heightPixels/15,0,0);
        parent2.setLayoutParams(params);

        RelativeLayout.LayoutParams p_priceparams = (RelativeLayout.LayoutParams) p_price.getLayoutParams();
        p_priceparams.setMargins(0,metrics.heightPixels/40,0,0);
        p_price.setLayoutParams(p_priceparams);

        RelativeLayout.LayoutParams s_priceparams = (RelativeLayout.LayoutParams) s_price.getLayoutParams();
        s_priceparams.setMargins(0,metrics.heightPixels/40,0,0);
        s_price.setLayoutParams(s_priceparams);

        RelativeLayout.LayoutParams encasLayoutParams = (RelativeLayout.LayoutParams) encasement.getLayoutParams();
        encasLayoutParams.setMargins(0,metrics.heightPixels/40,0,0);
        encasement.setLayoutParams(encasLayoutParams);

        LinearLayout.LayoutParams barcodeparentLayoutParams = (LinearLayout.LayoutParams) barcodeparent.getLayoutParams();
        barcodeparentLayoutParams.setMargins(0,metrics.heightPixels/40,0,0);
        barcodeparent.setLayoutParams(barcodeparentLayoutParams);

    }

    private void changelistener(AppCompatEditText saleformat , AppCompatEditText purchaseformat,AppCompatEditText description,AppCompatEditText purchase_price,
                                AppCompatEditText sale_price,AppCompatEditText encasement ,AppCompatEditText barcode ){

        description.addTextChangedListener(this);
        purchase_price.addTextChangedListener(this);
        sale_price.addTextChangedListener(this);
        encasement.addTextChangedListener(this);
        barcode.addTextChangedListener(this);
        saleformat.addTextChangedListener(this);
        purchaseformat.addTextChangedListener(this);


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callBack.Tab2_Result(format2.getText().toString(),format1.getText().toString(),description.getText().toString(),purchase_price.getText().toString(),
                sale_price.getText().toString(),encasement.getText().toString(),barcode.getText().toString());
    }
}
