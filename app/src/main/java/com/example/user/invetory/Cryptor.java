package com.example.user.invetory;


import android.util.Base64;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Cryptor {

    private SecretKeySpec generatkey(String password)throws Exception{

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = password.getBytes("UTF-8");
        digest.update(bytes,0,bytes.length);
        byte[] key = digest.digest();
        SecretKeySpec sks = new SecretKeySpec(key , "AES");
        return sks;
    }
    public String Encrypt(String Data , String password){
        try {
            SecretKeySpec keyspec = generatkey(password);
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE,keyspec);
            byte[] value = cipher.doFinal(Data.getBytes());
            String stringvalue = Base64.encodeToString(value,Base64.DEFAULT);
            return stringvalue;

        } catch (Exception e) {
            return  e.getMessage();
        }
    }
    public String Decrypt(String output , String password){
        try {
            SecretKeySpec keyspec = generatkey(password);
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE,keyspec);
            byte[] decodevlue = Base64.decode(output,Base64.DEFAULT);
            byte[] value = cipher.doFinal(decodevlue);
            String stringvalue = new String(value);
            return stringvalue;

        } catch (Exception e) {
            return null;
        }
    }
}
