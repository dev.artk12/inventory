package com.example.user.invetory.DetailePage;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.invetory.ContentObject;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.LocalHelper;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;
import com.example.user.invetory.Ucrop;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class DetailActivity extends AppCompatActivity implements HandlerValueFragment {

    private static final int REQUEST_IMAGE = 1;

    private static final int REQUEST_PERMISSION = 102 ;
    private static final int REQUEST_PERMISSION2 = 102 ;

    Uri uri;
    ViewPager viewPager;
    CustomFragmentAdapter adapter;
    TabLayout tabLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    SwitchCompat switchCompat;
    ImageView img;
    AppCompatImageButton camera ,save ,delete , pdf;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Map<String,String> result;
    ContentObject object;
    RelativeLayout edtbutton;
    RequestQueue queue;
    String [] permissions = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean requestcheck = true;
    AppCompatTextView calender;

    private static String ID_KEY = "ID";
    private static String NAME_KEY = "Name";
    private static String STOCK_KEY = "Stock";
    private static String CREATOR_KEY = "Creator";
    private static String MODEL_KEY = "Model";
    private static String GROUPNAME_KEY = "Group_Name";
    private static String PURCHASEPRICE_KEY = "Purchase_Price";
    private static String SALEPRICE_KEY = "Sale_Price";
    private static String ENCASEMET_KEY = "Encasement";
    private static String BARCODE_KEY = "BarCode";
    private static String DESCRIPTION_KEY = "Description";
    private static String ENCASEMENT_KEY    =   "Encasement" ;
    private static String IMAGENAMR_KEY     =   "ImageName" ;
    private static String IMAGE_KEY = "IMAGE";
    private static String STOCKFORMAT_KEY   =   "StockFormat" ;
    private static String SALEFORMAT_KEY   =   "Sale_PriceFormat" ;
    private static String PURCHASEFORMAT_KEY   =   "Purchase_PriceFormat" ;
    private static String CALENDER_KEY   =   "Calendar" ;

    private static String KEY = "afsaneh";
    SweetAlertDialog dialog , savedialog;
    String Current_Languge = "en";
    private static String Langueg_key = "Languge";
    StringRequest request , saverequest;
    AlertDialog.Builder vollydialog;
    int color;
    ImageRequest imgRequest;


    int id;
    String Email;
    Output_files files;
    Cryptor cryptor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferences = PreferenceManager.getDefaultSharedPreferences(DetailActivity.this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme",0);

        switch (themepos){
            case 0 :
                setTheme(R.style.DefultTheme);
                color = ContextCompat.getColor(this,R.color.blue);
                break;
            case 1 :
                color = ContextCompat.getColor(this,R.color.red);
                setTheme(R.style.RedTheme);
                break;
            case 2 :
                color = ContextCompat.getColor(this,R.color.green);
                setTheme(R.style.GreenTheme);
                break;
            case 3:
                color = ContextCompat.getColor(this,R.color.night);
                setTheme(R.style.NightMode);
                break;
        }

        dialog = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        savedialog = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        dialog.getProgressHelper().setBarColor(color);
        savedialog.getProgressHelper().setBarColor(color);


        setContentView(R.layout.activity_detail);
        cryptor = new Cryptor();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collaps);
        switchCompat = (SwitchCompat) findViewById(R.id.switch1);
        img =(ImageView) findViewById(R.id.img);
        camera = (AppCompatImageButton) findViewById(R.id.camera);
        edtbutton = (RelativeLayout) findViewById(R.id.editbutton);
        save = (AppCompatImageButton)findViewById(R.id.save);
        delete = (AppCompatImageButton)findViewById(R.id.delete);
        pdf = (AppCompatImageButton)findViewById(R.id.pdf);
        calender = (AppCompatTextView) findViewById(R.id.calender);
        files = new Output_files(this);
        Email = files.read("email");
        dialog.setCancelable(false);
        savedialog.setCancelable(false);
        savedialog.setTitleText("Saving...");
        vollydialog = new AlertDialog.Builder(this);


        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("id");

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        result = new HashMap<>();
        object = new ContentObject();
        queue = Volley.newRequestQueue(this);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) viewPager.getLayoutParams();
        params.setMargins(0,metrics.heightPixels/5,0,0);

        viewPager.setLayoutParams(params);

        switchCompat.setChecked(false);

        pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boolean ExternalStorage = checkPermissionExternalStorage();

                if (ExternalStorage){
                    String name = result.get(NAME_KEY);
                    String stock = result.get(STOCK_KEY);
                    String creator = result.get(CREATOR_KEY);
                    String model = result.get(MODEL_KEY);
                    String group = result.get(GROUPNAME_KEY);
                    String barcode = result.get(BARCODE_KEY);
                    BitmapDrawable drawableCompat = (BitmapDrawable) img.getDrawable();

                    if (drawableCompat != null){

                        Bitmap bitmap = drawableCompat.getBitmap();

                        Factor factor = Factor.newInstance(name,creator,model,stock,group,barcode,bitmap);
                        getSupportFragmentManager().beginTransaction().
                                setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_in).
                                replace(android.R.id.content,factor).addToBackStack("myscreen").commit();

                    }else
                        Toast.makeText(DetailActivity.this, "Please Wait", Toast.LENGTH_SHORT).show();

                }else {
                    ActivityCompat.requestPermissions(DetailActivity.this,permissions,REQUEST_PERMISSION2);
                }


            }
        });

        delete.setVisibility(View.INVISIBLE);
        save.setVisibility(View.INVISIBLE);
        CameraButtonAnim(false);

        Request(new DetailBack() {
            @Override
            public void trancfar(ContentObject object) {
                adapter = new CustomFragmentAdapter(getSupportFragmentManager(), object,false);
                result.put(IMAGENAMR_KEY, object.getImageAddress());
                StringBuilder builder = new StringBuilder(object.getCalende());
                builder.insert(4,"/");
                builder.insert(7,"/");

                calender.setText(builder.toString());
                viewPager.setCurrentItem(0);
                viewPager.setAdapter(adapter);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putBoolean("tab2",true);
                editor.putBoolean("tab1",true);
                editor.putBoolean("checktab2",false);

                editor.apply();

                String name = result.get(NAME_KEY);
                String stock = result.get(STOCK_KEY);
                String creator = result.get(CREATOR_KEY);
                String model = result.get(MODEL_KEY);
                String group = result.get(GROUPNAME_KEY);
                String description = result.get(DESCRIPTION_KEY);
                String purchaseprice = result.get(PURCHASEPRICE_KEY);
                String saleprice = result.get(SALEPRICE_KEY);
                String encasement = result.get(ENCASEMENT_KEY);
                String barcode = result.get(BARCODE_KEY);
                String imagename = result.get(IMAGENAMR_KEY);
                String stockformat = result.get(STOCKFORMAT_KEY);
                String saleformat = result.get(SALEFORMAT_KEY);
                String purchaseformat = result.get(PURCHASEFORMAT_KEY);

                //String nameEncryption = cryptor.Encrypt(name,KEY);
                String stockEncryption = cryptor.Encrypt(stock,KEY);
                String creatorEncryption = cryptor.Encrypt(creator,KEY);
                String modelkEncryption = cryptor.Encrypt(model,KEY);
                String descriptionEncryption = cryptor.Encrypt(description,KEY);
                String purchasepriceEncryption = cryptor.Encrypt(purchaseprice,KEY);
                String salepriceEncryption = cryptor.Encrypt(saleprice,KEY);
                String encasementEncryption = cryptor.Encrypt(encasement,KEY);
                String barcodeEncryption = cryptor.Encrypt(barcode,KEY);
                String saleformatEncryption = cryptor.Encrypt(saleformat,KEY);
                String purchaseformattEncryption = cryptor.Encrypt(purchaseformat,KEY);
                String stockformatEncryption = cryptor.Encrypt(stockformat,KEY);


                BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                byte[] imagebytes = stream.toByteArray();
                String Image = Base64.encodeToString(imagebytes,Base64.DEFAULT);


                boolean check = checkfild(Image,name,stock,creator,model,group,description,purchaseprice,saleprice,encasement,barcode);

                if (check){
                    requestvalue(Email,name,stockEncryption,creatorEncryption,modelkEncryption,group,
                            purchasepriceEncryption,salepriceEncryption,encasementEncryption,Image,descriptionEncryption,
                            barcodeEncryption,imagename,stockformatEncryption,saleformatEncryption,purchaseformattEncryption);
                    savedialog.show();
                }else {
                    Toast.makeText(DetailActivity.this, "Please Fill All Fields", Toast.LENGTH_SHORT).show();
                }

            }
        });


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean camrabool = checkPermissionCamera();
                boolean ExternalStorage = checkPermissionExternalStorage();

                if (camrabool && ExternalStorage){
                    CameraRequest();
                }else {
                    ActivityCompat.requestPermissions(DetailActivity.this,permissions,REQUEST_PERMISSION);
                }

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteRequest(result.get(IMAGENAMR_KEY));
                finish();
                editor.putBoolean("tab2",true);
                editor.putBoolean("tab1",true);
                editor.putBoolean("checktab2",false);

                editor.apply();
            }
        });

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("Edit Mode", isChecked).apply();
                Animation edtanim ;
                if (isChecked){
                    edtanim = AnimationUtils.loadAnimation(DetailActivity.this, R.anim.scale_in);
                    delete.setAnimation(edtanim);
                    save.setAnimation(edtanim);
                    delete.setVisibility(View.VISIBLE);
                    save.setVisibility(View.VISIBLE);
                    adapter = new CustomFragmentAdapter(getSupportFragmentManager(), object,true);
                    viewPager.setCurrentItem(0);
                    viewPager.setAdapter(adapter);
                    CameraButtonAnim(isChecked);
                    editor.remove("Edit Mode").apply();
                }else if (!isChecked){
                    edtanim = AnimationUtils.loadAnimation(DetailActivity.this, R.anim.scale_out);
                    delete.setAnimation(edtanim);
                    save.setAnimation(edtanim);
                    delete.setVisibility(View.INVISIBLE);
                    save.setVisibility(View.INVISIBLE);
                    adapter = new CustomFragmentAdapter(getSupportFragmentManager(), object,false);
                    viewPager.setCurrentItem(0);
                    viewPager.setAdapter(adapter);
                    CameraButtonAnim(isChecked);
                    editor.remove("Edit Mode").apply();
                }

            }
        });
        vollydialog.setMessage("Please Check Your Network").setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                request.setShouldCache(false);
                request.setRetryPolicy(new DefaultRetryPolicy(
                        (int) TimeUnit.SECONDS.toMillis(15),
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(request);
                dialog = new SweetAlertDialog(DetailActivity.this,SweetAlertDialog.PROGRESS_TYPE);
                dialog.getProgressHelper().setBarColor(color);
                dialog.setTitleText("Loading...");
                dialog.setCancelable(false);
                dialog.show();
            }
        }).setNegativeButton("Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        vollydialog.setCancelable(false);

        collapsingToolbarLayout.getLayoutParams().height = metrics.heightPixels / 3;

        setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        NestedScrollView scrollView = (NestedScrollView) findViewById (R.id.nested);
        scrollView.setFillViewport (true);

    }

    public void requestvalue(final String email, final String name , final String stock
            , final String creator , final String model , final String group , final String Purchase_price,
                             final String Sale_price, final String encasement, final String photo,
                             final String des, final String barcode, final String imagename, final String stockformat, final String saleformat, final String purchaseformat) {

        String uri = "http://www.inventory-customer.com/source/Editfuckingvlaue.php";

        saverequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                savedialog.dismissWithAnimation();
                finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismissWithAnimation();
                vollydialog.setMessage("Please Check Your Network").setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saverequest.setShouldCache(false);
                        saverequest.setRetryPolicy(new DefaultRetryPolicy(
                                (int) TimeUnit.SECONDS.toMillis(15),
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queue.add(request);
                    }
                }).setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }).setCancelable(false).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> requestmap = new HashMap<>();

                requestmap.put("id",String.valueOf(id));
                requestmap.put("email",Email);
                requestmap.put("name",name);
                requestmap.put("stock",stock);
                requestmap.put("creator",creator);
                requestmap.put("model",model);
                requestmap.put("groupname",group);
                requestmap.put("purchas_price",Purchase_price);
                requestmap.put("sale_price",Sale_price);
                requestmap.put("encasement",encasement);
                requestmap.put("barcode",barcode);
                requestmap.put("description",des);
                requestmap.put("image",photo);
                requestmap.put("ImageName", imagename);
                requestmap.put("stockformat",stockformat);
                requestmap.put("saleformat",saleformat);
                requestmap.put("purchaseformat", purchaseformat);

                return requestmap;
            }
        };

        saverequest.setShouldCache(false);
        saverequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(saverequest);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));
    }

    private boolean checkfild(String image, String name, String stock, String creator, String model,
                              String group, String description, String purchaseprice, String saleprice, String encasement, String barcode) {


        if (!name.isEmpty()&& !stock.isEmpty() && !creator.isEmpty()&& !model.isEmpty()&& !group.isEmpty()&& !image.equals("") && image.length() > 100 && !image.isEmpty() &&
                !description.isEmpty()&& !purchaseprice.isEmpty()&& !saleprice.isEmpty()&& !encasement.isEmpty()&& !barcode.isEmpty()) {

            return true;
        }else
            return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Ucrop ucrop = new Ucrop(color);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            ucrop.startCropActivity(DetailActivity.this,uri,"object");
        }else if (requestCode == REQUEST_IMAGE && resultCode == RESULT_CANCELED){
            switchCompat.setChecked(false);
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Bitmap bitmap = Ucrop.handleCropResult(this,data);
            img.setImageBitmap(bitmap);
            editor.putBoolean("Edit Mode", false);
            editor.putBoolean("image",false);
            editor.apply();
            switchCompat.setChecked(false);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            byte[] imagebytes = stream.toByteArray();
            String Image = Base64.encodeToString(imagebytes,Base64.DEFAULT);
            result.put(IMAGE_KEY,Image);
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            CameraRequest();
        }else if (requestCode == REQUEST_PERMISSION2 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            String name = result.get(NAME_KEY);
            String stock = result.get(STOCK_KEY);
            String creator = result.get(CREATOR_KEY);
            String model = result.get(MODEL_KEY);
            String group = result.get(GROUPNAME_KEY);
            String barcode = result.get(BARCODE_KEY);
            BitmapDrawable drawableCompat = (BitmapDrawable) img.getDrawable();

            if (drawableCompat != null){

                Bitmap bitmap = drawableCompat.getBitmap();

                Factor factor = Factor.newInstance(name,creator,model,stock,group,barcode,bitmap);
                getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_in).
                        replace(android.R.id.content,factor).addToBackStack("myscreen").commit();

            }else
                Toast.makeText(DetailActivity.this, "Please Wait", Toast.LENGTH_SHORT).show();

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void Tab1_Values(String name, String stock, String creator, String model, String purcahes_price,
                            String sale_price , String stockformat , String saleformat , String purchaseformat) {
        result.put(NAME_KEY,name);
        result.put(STOCK_KEY,stock);
        result.put(CREATOR_KEY,creator);
        result.put(MODEL_KEY,model);
        result.put(PURCHASEPRICE_KEY,purcahes_price);
        result.put(SALEPRICE_KEY,sale_price);
        result.put(STOCKFORMAT_KEY,stockformat);
        result.put(SALEFORMAT_KEY,saleformat);
        result.put(PURCHASEFORMAT_KEY,purchaseformat);

    }

    @Override
    public void Tab2_Values(String description ,String barcode , String encasement, String groupname) {
        result.put(DESCRIPTION_KEY,description);
        result.put(BARCODE_KEY,barcode);
        result.put(ENCASEMET_KEY,encasement);
        result.put(GROUPNAME_KEY,groupname);
    }

    private void DeleteRequest(final String ImageAddress) {
        String url  = "http://www.inventory-customer.com/source/delet.php" ;

        StringRequest deleterequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(DetailActivity.this, response.trim(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismissWithAnimation();
                Toast.makeText(DetailActivity.this, "Please Check Your Network", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> deletemap = new HashMap<>();
                deletemap.put("ID",String.valueOf(id));
                deletemap.put("email",Email);
                deletemap.put("imageaddress",ImageAddress);

                return deletemap;
            }
        };
        deleterequest.setShouldCache(false);
        deleterequest.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(15),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(deleterequest);
    }

    private void CameraRequest() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/inventory/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        startActivityForResult(CamIntent,REQUEST_IMAGE);
    }

    private void Set_Up(boolean Edit_Mode, boolean Imagebool) {
        Animation edtanim;
        if (Edit_Mode) {
            edtanim = AnimationUtils.loadAnimation(DetailActivity.this, R.anim.scale_in);
            delete.setAnimation(edtanim);
            save.setAnimation(edtanim);
            edtbutton.setVisibility(View.VISIBLE);
            delete.setVisibility(View.VISIBLE);
            save.setVisibility(View.VISIBLE);
            adapter = new CustomFragmentAdapter(getSupportFragmentManager(), object,false);
            viewPager.setCurrentItem(0);
            viewPager.setAdapter(adapter);
            editor.remove("Edit Mode").apply();
        } else {

            Request(new DetailBack() {
                @Override
                public void trancfar(ContentObject object) {
                    adapter = new CustomFragmentAdapter(getSupportFragmentManager(), object,false);
                    result.put(IMAGENAMR_KEY, object.getImageAddress());
                    viewPager.setCurrentItem(0);
                    viewPager.setAdapter(adapter);
                    editor.remove("image").apply();
                    requestcheck = false;

                }
            });

        edtanim = AnimationUtils.loadAnimation(DetailActivity.this, R.anim.scale_out);
        delete.setAnimation(edtanim);
        save.setAnimation(edtanim);
        delete.setVisibility(View.INVISIBLE);
        save.setVisibility(View.INVISIBLE);
        edtbutton.setVisibility(View.INVISIBLE);
    }
    }

    private void CameraButtonAnim(boolean Edit_Mode) {
        Animation animation;
        animation = AnimationUtils.loadAnimation(DetailActivity.this,android.R.anim.fade_out);
        camera.setVisibility(View.GONE);
        camera.setAnimation(animation);

        if(Edit_Mode){
            camera.setVisibility(View.VISIBLE);
            animation = AnimationUtils.loadAnimation(DetailActivity.this,android.R.anim.fade_in);
            camera.setAnimation(animation);

        }
    }

    public void Request( final DetailBack detailBack ){
        dialog.setTitleText("Loading...").show();

        String uri = "http://www.inventory-customer.com/source/EtitValue.php";

        request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    int id = jsonObject.getInt(ID_KEY);
                    String name = jsonObject.getString(NAME_KEY);
                    String stock = cryptor.Decrypt(jsonObject.getString(STOCK_KEY),KEY);
                    String barcode = cryptor.Decrypt(jsonObject.getString(BARCODE_KEY),KEY);
                    String creator = cryptor.Decrypt(jsonObject.getString(CREATOR_KEY),KEY);
                    String description = cryptor.Decrypt(jsonObject.getString(DESCRIPTION_KEY),KEY);
                    String encasement = cryptor.Decrypt(jsonObject.getString(ENCASEMENT_KEY),KEY);
                    String model = cryptor.Decrypt(jsonObject.getString(MODEL_KEY),KEY);
                    String saleprice = cryptor.Decrypt(jsonObject.getString(SALEPRICE_KEY),KEY);
                    String stockformat = cryptor.Decrypt(jsonObject.getString(STOCKFORMAT_KEY),KEY);
                    String saleformat = cryptor.Decrypt(jsonObject.getString(SALEFORMAT_KEY),KEY);
                    String purcahseprice = cryptor.Decrypt(jsonObject.getString(PURCHASEPRICE_KEY),KEY);
                    String purchaseformat = cryptor.Decrypt(jsonObject.getString(PURCHASEFORMAT_KEY),KEY);
                    String calender = jsonObject.getString(CALENDER_KEY);

                    object.setID(jsonObject.getInt(ID_KEY));
                    object.setName(name);
                    object.setCalende(calender);
                    object.setStock(stock);
                    object.setBarCode(barcode);
                    object.setCreator(creator);
                    object.setDescription(description);
                    object.setEncasement(encasement);
                    object.setStockFormat(stockformat);
                    object.setSaleFormat(saleformat);
                    object.setPurchaseFomat(purchaseformat);
                    object.setGroup_Name(jsonObject.getString(GROUPNAME_KEY));
                    object.setModel(model);
                    object.setImageAddress(jsonObject.getString(IMAGENAMR_KEY));
                    object.setPurchase_Price(purcahseprice);
                    object.setSale_Price(saleprice);

                    detailBack.trancfar(object);
                    dialog.dismissWithAnimation();

                    imgRequest = new ImageRequest("http://www.inventory-customer.com/"+object.getImageAddress(), new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            img.setImageBitmap(response);
                        }
                    }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    RequestQueue queue = Volley.newRequestQueue(DetailActivity.this);
                                    imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    queue.add(imgRequest);
                                }
                            });
                    RequestQueue queue = Volley.newRequestQueue(DetailActivity.this);
                    imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queue.add(imgRequest);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismissWithAnimation();
                vollydialog.show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> names = new HashMap<>();
                names.put("id",String.valueOf(id));
                names.put("email",Email);
                return names;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(15),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    boolean checkPermissionCamera(){
        int res = checkCallingOrSelfPermission(permissions[0]);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    boolean checkPermissionExternalStorage(){
        int res = checkCallingOrSelfPermission(permissions[1]);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public class CustomFragmentAdapter extends FragmentPagerAdapter{

        String name;
        String stock ;
        String creator ;
        String model ;
        String p_price ;
        String s_price ;
        String groupname;
        String description;
        String encasement;
        String barcode;
        String stockformat;
        String saleformat;
        String purchaseformat;
        boolean Enable;

        public CustomFragmentAdapter(FragmentManager fm,ContentObject objec,boolean Enable) {
            super(fm);
            this.name = objec.getName();
            this.stock = objec.getStock();
            this.creator = objec.getCreator();
            this.model = objec.getModel();
            this.p_price = objec.getPurchase_Price();
            this.s_price = objec.getSale_Price();
            this.groupname = objec.getGroup_Name();
            this.description = objec.getDescription();
            this.encasement = objec.getEncasement();
            this.barcode = objec.getBarCode();
            this.stockformat = objec.getStockFormat();
            this.saleformat = objec.getSaleFormat();
            this.purchaseformat = objec.getPurchaseFomat();
            this.Enable = Enable;

        }

        @Override
        public Fragment getItem(int position)  {

            switch (position){
                case 0 : DetailTab1 detailTab1 = DetailTab1.newInstance(name,stock,creator,model,
                        p_price,s_price,stockformat,saleformat,purchaseformat,Enable); return detailTab1;
                case 1 : DetailTab2 detailTab2 = DetailTab2.newInstance(description,barcode,encasement,groupname,Enable) ; return detailTab2;
                case 2 : DetailTab3 detailTab3 = DetailTab3.newInstance(Email,String.valueOf(id)); return detailTab3;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}

interface DetailBack{
    void trancfar(ContentObject object);
}
