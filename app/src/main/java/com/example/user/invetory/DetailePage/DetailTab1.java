package com.example.user.invetory.DetailePage;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.invetory.R;
import com.example.user.invetory.spinneradapter;

import java.util.ArrayList;


public class DetailTab1 extends Fragment implements TextWatcher {


    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    EditText name , stock , creator , model ,parchase_price , sale_price , format1 , format2 , format3;
    boolean Enable;
    int color;
    LinearLayout parent;
    AppCompatImageButton stockimg,saleimg,purchaseimg;
    RecyclerView stocklist,purchaselist,salelist;

    private static int TAG_ONE = 0 ;
    private static int TAG_TWO = 0 ;
    private static int TAG_THREE = 0 ;

    ArrayList<String> names;
    Animation listanim, iconanim , fadein , fadeout;

    private static String NAME_KEY = "Name";
    private static String STOCK_KEY = "Stock";
    private static String CREATOR_KEY = "Creator";
    private static String MODEL_KEY = "Model";
    private static String SALEPRICE_KEY = "Sale_Price";
    private static String STOCKFORMAT_KEY = "StockFormat";
    private static String SALEFORMAT_KEY = "SaleFormat";
    private static String PURCHASEPRICE_KEY = "Purchase_Price";
    private static String PURCHASEFORMAT_KEY = "PurchaseFormat";
    private static String ENABLE_KEY = "ENABLE";


    HandlerValueFragment values;


    public static DetailTab1 newInstance(String name , String stock , String creator , String model , String p_price ,
                                          String s_price, String stockformat,String saleformat,String purchaseformat,boolean enable) {
        DetailTab1 fragment = new DetailTab1();
        Bundle args = new Bundle();
        args.putString(NAME_KEY,name);
        args.putString(STOCK_KEY,stock);
        args.putString(CREATOR_KEY,creator);
        args.putString(MODEL_KEY,model);
        args.putString(PURCHASEPRICE_KEY,p_price);
        args.putString(SALEPRICE_KEY,s_price);
        args.putString(STOCKFORMAT_KEY,stockformat);
        args.putString(SALEFORMAT_KEY,saleformat);
        args.putString(PURCHASEFORMAT_KEY,purchaseformat);
        args.putBoolean(ENABLE_KEY,enable);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        values = (HandlerValueFragment) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();


        View view = inflater.inflate(R.layout.detailtab1,container,false);
        name = (EditText) view.findViewById(R.id.name);
        stock = (EditText) view.findViewById(R.id.stock);
        creator = (EditText) view.findViewById(R.id.Creator);
        model = (EditText) view.findViewById(R.id.model);
        parchase_price = (EditText) view.findViewById(R.id.p_price);
        sale_price = (EditText) view.findViewById(R.id.s_price);

        stockimg = (AppCompatImageButton) view.findViewById(R.id.dropdownstock);
        stocklist = (RecyclerView) view.findViewById(R.id.listviewstock);
        format1 = (AppCompatEditText) view.findViewById(R.id.formatstock);

        purchaseimg = (AppCompatImageButton) view.findViewById(R.id.dropdownpurchase);
        purchaselist = (RecyclerView) view.findViewById(R.id.listviewpurchase);
        format2 = (AppCompatEditText) view.findViewById(R.id.formatpurchase);

        saleimg = (AppCompatImageButton) view.findViewById(R.id.dropdownsale);
        salelist = (RecyclerView) view.findViewById(R.id.listviewsale);
        format3 = (AppCompatEditText) view.findViewById(R.id.formatsale);

        format1.setEnabled(false);
        format2.setEnabled(false);
        format3.setEnabled(false);


        int themepos = preferences.getInt("theme",0);

        switch (themepos){
            case 0 : color = ContextCompat.getColor(getContext(),R.color.blue); break;
            case 1 : color = ContextCompat.getColor(getContext(),R.color.red);break;
            case 2 : color = ContextCompat.getColor(getContext(),R.color.green);break;
            case 3 : color = ContextCompat.getColor(getContext(),R.color.Tab_background);break;
        }

        stockimg.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        saleimg.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        purchaseimg.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        ArrayList<String> names;

        names = new ArrayList<>();
        names.add("hel");
        names.add("fuc");
        names.add("cd");
        names.add("ddd");

        spinneradapter arrayAdapter1 = new spinneradapter(true,getContext(),format1,names);
        spinneradapter arrayAdapter2 = new spinneradapter(true,getContext(),format2,names);
        spinneradapter arrayAdapter3 = new spinneradapter(true,getContext(),format3,names);

        stocklist.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        stocklist.setAdapter(arrayAdapter1);

        purchaselist.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        purchaselist.setAdapter(arrayAdapter2);

        salelist.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        salelist.setAdapter(arrayAdapter3);

        stockimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TAG_ONE == 0){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopen);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerlist);
                    stocklist.setVisibility(View.VISIBLE);
                    stocklist.startAnimation(listanim);
                    stocklist.setEnabled(true);
                    stockimg.startAnimation(iconanim);
                    format1.setEnabled(false);
                    TAG_ONE = 1;
                }else if (TAG_ONE == 1){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclose);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinneredt);
                    stocklist.setVisibility(View.INVISIBLE);
                    stocklist.setEnabled(false);
                    stocklist.startAnimation(listanim);
                    stockimg.startAnimation(iconanim);
                    format1.setEnabled(true);
                    format1.requestFocus();
                    TAG_ONE = 2;
                }else if (TAG_ONE == 2){
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerclose);
                    stockimg.startAnimation(iconanim);
                    stocklist.setEnabled(false);
                    format1.setEnabled(false);
                    TAG_ONE = 0;
                }
            }
        });


        purchaseimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TAG_TWO == 0){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopen);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerlist);
                    purchaselist.setVisibility(View.VISIBLE);
                    purchaselist.startAnimation(listanim);
                    purchaselist.setEnabled(true);
                    purchaseimg.startAnimation(iconanim);
                    format2.setEnabled(false);
                    TAG_TWO = 1;
                }else if (TAG_TWO == 1){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclose);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinneredt);
                    purchaselist.setVisibility(View.INVISIBLE);
                    purchaselist.setEnabled(false);
                    purchaselist.startAnimation(listanim);
                    purchaseimg.startAnimation(iconanim);
                    format2.setEnabled(true);
                    format2.requestFocus();
                    TAG_TWO = 2;
                }else if (TAG_TWO == 2){
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerclose);
                    purchaseimg.startAnimation(iconanim);
                    purchaselist.setEnabled(false);
                    format2.setEnabled(false);
                    TAG_TWO = 0;
                }
            }
        });



        saleimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TAG_THREE == 0){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaopen);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerlist);
                    salelist.setVisibility(View.VISIBLE);
                    salelist.startAnimation(listanim);
                    salelist.setEnabled(true);
                    saleimg.startAnimation(iconanim);
                    format3.setEnabled(false);
                    TAG_THREE = 1;
                }else if (TAG_THREE == 1){
                    listanim = AnimationUtils.loadAnimation(getContext(),R.anim.spinneranimaclose);
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinneredt);
                    salelist.setVisibility(View.INVISIBLE);
                    salelist.setEnabled(false);
                    salelist.startAnimation(listanim);
                    saleimg.startAnimation(iconanim);
                    format3.setEnabled(true);
                    format3.requestFocus();
                    TAG_THREE = 2;
                }else if (TAG_THREE == 2){
                    iconanim = AnimationUtils.loadAnimation(getContext(),R.anim.rotateiconspinnerclose);
                    saleimg.startAnimation(iconanim);
                    salelist.setEnabled(false);
                    format3.setEnabled(false);
                    TAG_THREE = 0;
                }
            }
        });



        Enable = preferences.getBoolean("Edit Mode",false);//getArguments().getBoolean(ENABLE_KEY);

        String sname = getArguments().getString(NAME_KEY);
        String sstock = getArguments().getString(STOCK_KEY);
        String screator = getArguments().getString(CREATOR_KEY);
        String smodel = getArguments().getString(MODEL_KEY);
        String sp_price = getArguments().getString(PURCHASEPRICE_KEY);
        String ss_price = getArguments().getString(SALEPRICE_KEY);
        String stockformat = getArguments().getString(STOCKFORMAT_KEY);
        String saleformat = getArguments().getString(SALEFORMAT_KEY);
        String purchaseformat = getArguments().getString(PURCHASEFORMAT_KEY);


        name.setText(sname);
        stock.setText(sstock);
        creator.setText(screator);
        model.setText(smodel);
        parchase_price.setText(sp_price);
        sale_price.setText(ss_price);
        format1.setText(stockformat);
        format2.setText(purchaseformat);
        format3.setText(saleformat);

        EditMode_Mood(Enable);
        values.Tab1_Values(name.getText().toString(),stock.getText().toString(),creator.getText().toString(),
                model.getText().toString(),parchase_price.getText().toString(),sale_price.getText().toString(),format1.getText().toString(),
                format3.getText().toString(),format2.getText().toString());

        updateinterface();

        return view;
    }
    private void updateinterface(){

        name.addTextChangedListener(this);
        stock.addTextChangedListener(this);
        creator.addTextChangedListener(this);
        model.addTextChangedListener(this);
        parchase_price.addTextChangedListener(this);
        sale_price.addTextChangedListener(this);
        format1.addTextChangedListener(this);
        format2.addTextChangedListener(this);
        format3.addTextChangedListener(this);


    }
    private void EditMode_Mood(boolean Enable) {
        name.setEnabled(Enable);
        stock.setEnabled(Enable);
        creator.setEnabled(Enable);
        model.setEnabled(Enable);
        parchase_price.setEnabled(Enable);
        sale_price.setEnabled(Enable);
        stockimg.setEnabled(Enable);
        saleimg.setEnabled(Enable);
        purchaseimg.setEnabled(Enable);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        values.Tab1_Values(name.getText().toString(),stock.getText().toString(),creator.getText().toString(), model.getText().toString(),
                parchase_price.getText().toString(),sale_price.getText().toString(),format1.getText().toString(),
                format3.getText().toString(),format2.getText().toString());

    }
}

