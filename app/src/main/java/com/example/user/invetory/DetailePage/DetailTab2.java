package com.example.user.invetory.DetailePage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.invetory.R;


public class DetailTab2 extends Fragment implements TextWatcher {

    HandlerValueFragment values;

    EditText description,barcode,encasement,groupname;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean Enable;

    private static String ENCASEMET_KEY = "Encasement";
    private static String BARCODE_KEY = "BarCode";
    private static String DESCRIPTION_KEY = "Description";
    private static String GROUPNAME_KEY = "Group_Name";
    private static String ENABLE_KEY = "ENABLE";



    public static DetailTab2 newInstance(String description , String barcode , String encasement , String groupname,boolean Enable){
        DetailTab2 fragment = new DetailTab2();

        Bundle args = new Bundle();
        args.putString(DESCRIPTION_KEY,description);
        args.putString(BARCODE_KEY,barcode);
        args.putString(ENCASEMET_KEY,encasement);
        args.putString(GROUPNAME_KEY,groupname);
        args.putBoolean(ENABLE_KEY,Enable);
        fragment.setArguments(args);


        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        values = (HandlerValueFragment) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detailtab2,container,false);
        description = (EditText)view.findViewById(R.id.descripton);
        barcode = (EditText)view.findViewById(R.id.barcode);
        encasement = (EditText) view.findViewById(R.id.encasement);
        groupname = (EditText) view.findViewById(R.id.g_name);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();

        Enable = preferences.getBoolean("Edit Mode",false);//getArguments().getBoolean(ENABLE_KEY);
        String sdescription = getArguments().getString(DESCRIPTION_KEY);
        String sbarcode = getArguments().getString(BARCODE_KEY);
        String sencasement = getArguments().getString(ENCASEMET_KEY);
        String sgroupname = getArguments().getString(GROUPNAME_KEY);


        description.setText(sdescription);
        barcode.setText(sbarcode);
        encasement.setText(sencasement);
        groupname.setText(sgroupname);

        Editmode(Enable);

        values.Tab2_Values(description.getText().toString(),barcode.getText().toString(),encasement.getText().toString(),groupname.getText().toString());

        updateinterface(description,barcode,encasement);


        return view;
    }

    private void updateinterface(EditText description, EditText barcode ,EditText encasement){

        description.addTextChangedListener(this);
        barcode.addTextChangedListener(this);
        encasement.addTextChangedListener(this);
        groupname.addTextChangedListener(this);


    }

    private void Editmode(boolean Enable) {
        description.setEnabled(Enable);
        barcode.setEnabled(Enable);
        encasement.setEnabled(Enable);
        groupname.setEnabled(Enable);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        values.Tab2_Values(description.getText().toString(),barcode.getText().toString(),encasement.getText().toString(),groupname.getText().toString());
    }
}
