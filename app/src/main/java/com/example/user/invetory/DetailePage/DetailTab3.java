package com.example.user.invetory.DetailePage;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.R;
import com.example.user.invetory.TransAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class DetailTab3 extends Fragment {

    RequestQueue queue;
    StringRequest request;
    private static String EMAILKEY = "Email";
    private static String OBJECTKEY = "Object";
    String Email , objectid;
    ArrayList<TransAction> list;

    private static String ID                = "ID";
    private static String OBJECTID          = "ObjectID";
    private static String PAYMENTMETKOD = "PaymentMethod";
    private static String PRICE             = "Price";
    private static String SALEAMOUNT        = "Sales_Amount";
    private static String TRANSACTIONSERIAL = "TransActionSerial";
    private static String BUYORSELL         = "BuyOrSale";
    private static String KEY = "afsaneh";
    TransActionAdapter adapter;

    Cryptor cryptor;

    public static DetailTab3 newInstance(String Email , String objectid) {

        Bundle args = new Bundle();
        args.putString(EMAILKEY,Email);
        args.putString(OBJECTKEY,objectid);

        DetailTab3 fragment = new DetailTab3();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        queue = Volley.newRequestQueue(getContext());
        list = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_tab3,container,false);

        String url = "http://www.inventory-customer.com/source/GetTransActions.php";

        final RecyclerView transActionlist = (RecyclerView) view.findViewById(R.id.transActionlist);


        cryptor = new Cryptor();

        Email = getArguments().getString(EMAILKEY);
        objectid = getArguments().getString(OBJECTKEY);


        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length() ; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        TransAction transAction = new TransAction();


                        transAction.setId(jsonObject.getString(ID));
                        switch (jsonObject.getString(BUYORSELL)){
                            case "0" : transAction.setBuyOrSell("Sell"); break;
                            case "1" : transAction.setBuyOrSell("Buy"); break;

                        }
                        transAction.setObjectId(jsonObject.getString(OBJECTID));

                        switch (jsonObject.getString(PAYMENTMETKOD)){
                            case "0" : transAction.setPeymentMethod("Cash"); break;
                            case "1" : transAction.setPeymentMethod("Cheque"); break;
                            case "2" : transAction.setPeymentMethod("Card"); break;
                        }
                        transAction.setPrice(cryptor.Decrypt(jsonObject.getString(PRICE),KEY));
                        transAction.setSaleAmount(cryptor.Decrypt(jsonObject.getString(SALEAMOUNT),KEY));
                        transAction.setTransActionsSerial(cryptor.Decrypt(jsonObject.getString(TRANSACTIONSERIAL),KEY));

                        list.add(transAction);
                    }

                    RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
                    transActionlist.setLayoutManager(manager);
                    transActionlist.setItemAnimator(new DefaultItemAnimator());
                    adapter = new TransActionAdapter(list,getContext());
                    transActionlist.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map = new HashMap<>();

                map.put("email",Email);
                map.put("objectid",objectid);

                return map;
            }
        };


        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

        return view;
    }


}
