package com.example.user.invetory.DetailePage;

import android.Manifest;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.invetory.R;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

public class Factor extends Fragment {

    private static String NAME_KEY = "Name";
    private static String CREATOE_KEY = "Creator";
    private static String MODEL_KEY = "Model";
    private static String STOCK_KEY = "Stock";
    private static String GROUP_KEY = "Group";
    private static String BARCODE_KEY = "Barcode";
    private static String IMAGE_KEY = "Image";

    AppCompatImageView img ;
    TextView Name , Creator , Model , Stock , Barcode , Group;


    public static Factor newInstance(String Name , String Creator , String Model , String Stock , String Group , String Barcode , Bitmap image) {

        Bundle args = new Bundle();
        args.putString(NAME_KEY,Name);
        args.putString(CREATOE_KEY,Creator);
        args.putString(MODEL_KEY,Model);
        args.putString(STOCK_KEY,Stock);
        args.putString(GROUP_KEY,Group);
        args.putString(BARCODE_KEY,Barcode);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG,100,stream);
        args.putByteArray(IMAGE_KEY,stream.toByteArray());

        Factor fragment = new Factor();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.factor,container,false);
        Name = (TextView) view.findViewById(R.id.description_Name);
        Stock = (TextView) view.findViewById(R.id.description_Stock);
        Creator = (TextView) view.findViewById(R.id.description_Creator);
        Model = (TextView) view.findViewById(R.id.description_Model);
        Barcode = (TextView) view.findViewById(R.id.description_Barcode);
        Group = (TextView) view.findViewById(R.id.description_Group);
        img = (AppCompatImageView) view.findViewById(R.id.img);

        final String name = getArguments().getString(NAME_KEY);
        String creator = getArguments().getString(CREATOE_KEY);
        String model = getArguments().getString(MODEL_KEY);
        String stock = getArguments().getString(STOCK_KEY);
        final String barcode = getArguments().getString(BARCODE_KEY);
        String group = getArguments().getString(GROUP_KEY);
        byte[] imagebytes = getArguments().getByteArray(IMAGE_KEY);

        Bitmap bitmap = BitmapFactory.decodeByteArray(imagebytes,0,imagebytes.length);

        Name.setText(name);
        Stock.setText(stock);
        Creator.setText(creator);
        Model.setText(model);
        Barcode.setText(barcode);
        Group.setText(group);
        img.setImageBitmap(bitmap);




        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},00);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = screenShot(getActivity().getWindow().getDecorView().getRootView());
                String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Inventory/PDF";
                File child = new File(path);
                try {
                    FileOutputStream outputStream = new FileOutputStream(child+"/"+name+barcode+".pdf");

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);

                    Document document = new Document();
                    PdfWriter.getInstance(document,outputStream);
                    document.open();
                    Image image = Image.getInstance(stream.toByteArray());
                    float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                            - document.rightMargin() - 0) / image.getWidth()) * 80; // 0 means you have no indentation. If you have any, change it.
                    image.scalePercent(scaler);
                    image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                    document.add(image);
                    document.close();

                } catch (FileNotFoundException e) {
                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                } catch (MalformedURLException e) {
                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                } catch (BadElementException e) {
                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                } catch (DocumentException e) {
                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, 1000);

        return view;

    }

    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
}
