package com.example.user.invetory.DetailePage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.invetory.R;
import com.example.user.invetory.TransAction;
import java.util.ArrayList;

public class TransActionAdapter extends RecyclerView.Adapter<TransActionAdapter.ViewHolder>{

    ArrayList<TransAction> list ;
    Context context;

    public TransActionAdapter (ArrayList<TransAction> list , Context context){
        this.context = context;
        if (list == null){
            list = new ArrayList<>();
        }
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.tranactionlisitems,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.BuyOrSell.setText(list.get(i).getBuyOrSell());
        viewHolder.Price.setText(list.get(i).getPrice());
        viewHolder.PayMethod.setText(list.get(i).getPeymentMethod());
        viewHolder.TransActionserial.setText(list.get(i).getTransActionsSerial());
        viewHolder.Saleamount.setText(list.get(i).getSaleAmount());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView BuyOrSell , Price , PayMethod , TransActionserial , Saleamount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            BuyOrSell = (TextView) itemView.findViewById(R.id.BuyOrSell);
            Price = (TextView) itemView.findViewById(R.id.Price);
            PayMethod = (TextView) itemView.findViewById(R.id.paymentmethod);
            TransActionserial = (TextView) itemView.findViewById(R.id.transactionserial);
            Saleamount = (TextView) itemView.findViewById(R.id.saleamount);

        }
    }
}
