package com.example.user.invetory.ForgetPass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.example.user.invetory.BaseActivitysAndFragmensNeed.Entrance;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.MainActivity;
import com.example.user.invetory.Profile.Profile;
import com.example.user.invetory.R;
import com.example.user.invetory.SignUp.CustomViewPager_SignUp;
import com.example.user.invetory.SignUp.SignUpBaseActivity;
import com.example.user.invetory.SignUp.Sign_UP_Tab1;
import com.example.user.invetory.SignUp.Sign_UP_Tab2;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ForgetPassBaseActivity extends AppCompatActivity implements Tab1ForgetCalBack , Tab2ForgetCalBack{

    public static CustomViewPager_ForgetPass viewPager_forgetPass;
    forgetpassadapter adapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int color;
    Map<String,String> result;
    Cryptor cryptor;
    RequestQueue queue;
    private static String ENCRYPT_KEY = "afsaneh";
    SweetAlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleSSLHandshake();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme", 0);

        switch (themepos) {
            case 0:
                color = ContextCompat.getColor(this,R.color.snblue);
                setTheme(R.style.DefultTheme);
                break;
            case 1:
                color = ContextCompat.getColor(this,R.color.snred);
                setTheme(R.style.RedTheme);
                break;
            case 2:
                color = ContextCompat.getColor(this,R.color.sngreen);
                setTheme(R.style.GreenTheme);
                break;
            case 3:
                color = ContextCompat.getColor(this,R.color.night);
                setTheme(R.style.NightMode);
        }
        setContentView(R.layout.activity_forget_pass_base);
        viewPager_forgetPass = (CustomViewPager_ForgetPass) findViewById(R.id.viewpager_forgetpass);

        adapter = new forgetpassadapter(getSupportFragmentManager());
        viewPager_forgetPass.setAdapter(adapter);
        result = new HashMap<>();
        cryptor = new Cryptor();
        queue = Volley.newRequestQueue(this);

    }

    @Override
    public void callback(String Email) {
        result.put("email",Email);
    }

    @Override
    public void callbacktab2(String password) {
        result.put("password",password);

    }

    @Override
    public void finishButton(final AppCompatButton finish) {
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish.setEnabled(false);
                dialog = new SweetAlertDialog(ForgetPassBaseActivity.this,SweetAlertDialog.PROGRESS_TYPE);
                dialog.setTitleText("Please wait");
                dialog.setCancelable(false);
                dialog.show();
                StringRequest request = new StringRequest(Request.Method.POST, "http://www.inventory-customer.com/source/passwordchange.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismissWithAnimation();
                        Intent intent = new Intent(ForgetPassBaseActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Somthing wrong...", TSnackbar.LENGTH_SHORT);
                        View view = tSnackbar.getView();
                        view.setBackgroundColor(Color.parseColor("#dedede"));
                        TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                        textView.setTextColor(color);
                        tSnackbar.show();

                        finish.setEnabled(true);


                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String , String> params = new HashMap<>();
                        params.put("email" , result.get("email"));
                        String password = cryptor.Encrypt(result.get("password"),ENCRYPT_KEY);
                        params.put("password",password);
                        return params;
                    }
                };
                request.setShouldCache(false);
                request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(request);

            }
        });
    }

    private class forgetpassadapter extends FragmentPagerAdapter{

        public forgetpassadapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i){
                case 0 : return new forgetpasstab1();
                case 1 : return new forgetpasstab2();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

    }
    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(ForgetPassBaseActivity.this,MainActivity.class));
    }
}
