package com.example.user.invetory.ForgetPass;

import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.invetory.R;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class forgetpasstab1 extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    EditText email , code ;
    AppCompatButton btn;
    View view1,view2;
    RequestQueue queue;
    StringRequest request;
    Tab1ForgetCalBack callBack;
    private static int TAG = 0;


    private String mParam1;
    private String mParam2;



    public static forgetpasstab1 newInstance(String param1, String param2) {
        forgetpasstab1 fragment = new forgetpasstab1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (Tab1ForgetCalBack) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue = Volley.newRequestQueue(getContext());

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forgetpass_tab1,container,false);
        email = view.findViewById(R.id.emailforget);
        code = view.findViewById(R.id.code);
        btn = view.findViewById(R.id.forgetpassnext);

        view1 = view.findViewById(R.id.view1);
        view2 = view.findViewById(R.id.view2);

        btn.setEnabled(false);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn.setEnabled(false);
                email.setEnabled(false);
                btn.setText("Sending...");
                code.setEnabled(true);

                    request(email.getText().toString(), new vollycallbackforgetcode() {
                        @Override
                        public void callback(final String resultcode) {
                            btn.setText("Sended");

                            code.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (s.toString().equals(resultcode)){
                                        view2.setBackgroundResource(R.drawable.forgetpassgreen);
                                        ForgetPassBaseActivity.viewPager_forgetPass.setunLuck(true);
                                        TAG = 2;
                                        btn.setEnabled(false);
                                        code.setEnabled(false);
                                        ForgetPassBaseActivity.viewPager_forgetPass.setCurrentItem(ForgetPassBaseActivity.viewPager_forgetPass.getCurrentItem()+1);


                                    }else{
                                        view2.setBackgroundResource(R.drawable.forgetpassred);
                                        ForgetPassBaseActivity.viewPager_forgetPass.setunLuck(false);
                                        TAG = 3;
                                        btn.setEnabled(false);

                                    }

                                }
                            });
                        }
                    });

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()){
                    view1.setBackgroundResource(R.drawable.forgetpassgreen);
                    btn.setEnabled(true);
                    callBack.callback(s.toString());
                    TAG = 1;
                }else{
                    view1.setBackgroundResource(R.drawable.forgetpassred);
                    btn.setEnabled(false);
                    callBack.callback("null");
                    TAG = 0 ;
                }
            }
        });


        return view;
    }
    private void request(final String email , final vollycallbackforgetcode callback){
        request = new StringRequest(Request.Method.POST, "http://www.inventory-customer.com/source/EmailSend.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                callback.callback(response.trim());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                request.setShouldCache(false);
                queue.add(request);

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map map = new HashMap();
                map.put("email",email);
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        queue.add(request);

    }
    interface vollycallbackforgetcode{
        void callback(String code);
    }

}
