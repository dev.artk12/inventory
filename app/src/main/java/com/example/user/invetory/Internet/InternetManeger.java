package com.example.user.invetory.Internet;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class InternetManeger {

    public void saveindb(final Context context ,final String emailAddress , final String name,final String lastname,final String password,final String photo){
        String uri = "http://www.inventory-customer.com/source/users.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
              /* if (response == null){
                   Toast.makeText(context, "Registration was not successful: You have already registered", Toast.LENGTH_SHORT).show();
               }else if (response != null){
                   Toast.makeText(context, "Registration successful", Toast.LENGTH_SHORT).show();
               }*/

                Log.e("errrror",response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> getparams = new HashMap<>();
                getparams.put("emailAddress",emailAddress);
                getparams.put("name",name);
                getparams.put("password",password);
                getparams.put("photo",photo);

                return getparams;
            }
        };
        queue.add(request);
    }


}
