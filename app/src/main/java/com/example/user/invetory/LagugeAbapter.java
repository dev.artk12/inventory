package com.example.user.invetory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class LagugeAbapter extends ArrayAdapter {

    ArrayList<String> languge;
    ArrayList<String> langugeName;

    public LagugeAbapter(@NonNull Context context, ArrayList<String> languge,ArrayList<String> langugeName) {
        super(context, R.layout.langugeadpter,languge);
        this.languge = languge;
        this.langugeName = langugeName;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.langugeadpter,parent,false);
        TextView name = (TextView)view.findViewById(R.id.names);
        TextView langugename = (TextView)view.findViewById(R.id.language);

        name.setText(languge.get(position));
        langugename.setText(langugeName.get(position));

        return view;
    }
}
