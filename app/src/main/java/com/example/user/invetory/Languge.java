package com.example.user.invetory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.invetory.BaseActivitysAndFragmensNeed.Entrance;

import java.util.ArrayList;

public class Languge extends AppCompatActivity {

    ListView listView;
    LagugeAbapter abapter;
    ArrayList<String> languges;
    ArrayList<String> langugesNames;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String Current_Languge = "en";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme", 0);

        switch (themepos){
            case 0 : setTheme(R.style.DefultTheme); break;
            case 1 : setTheme(R.style.RedTheme); break;
            case 2 : setTheme(R.style.GreenTheme); break;
            case 3 : setTheme(R.style.NightMode); break;
        }
        setContentView(R.layout.activity_languge);
        listView = (ListView) findViewById(R.id.listview);
        langugesNamessetup();

        langugesetup();
        abapter = new LagugeAbapter(this,languges,langugesNames);
        listView.setAdapter(abapter);
        listView .setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0 :
                        LocalHelper.setLocal(Languge.this,"ar");
                        ChangeLanguge(Languge.this,"ar");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 1 :
                        LocalHelper.setLocal(Languge.this,"en");
                        ChangeLanguge(Languge.this,"en");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 2 :
                        LocalHelper.setLocal(Languge.this,"fr");
                        ChangeLanguge(Languge.this,"fr");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 3 :
                        LocalHelper.setLocal(Languge.this,"de");
                        ChangeLanguge(Languge.this,"de");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 4 :
                        LocalHelper.setLocal(Languge.this,"ko");
                        ChangeLanguge(Languge.this,"ko");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 5 :
                        LocalHelper.setLocal(Languge.this,"it");
                        ChangeLanguge(Languge.this,"it");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 6 :
                        LocalHelper.setLocal(Languge.this,"fa");
                        ChangeLanguge(Languge.this,"fa");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 7 :
                        LocalHelper.setLocal(Languge.this,"pt");
                        ChangeLanguge(Languge.this,"pt");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                    case 8 :
                        LocalHelper.setLocal(Languge.this,"es");
                        ChangeLanguge(Languge.this,"es");
                        finish();
                        startActivity(new Intent(Languge.this, Entrance.class)); break;
                }
            }
        });


    }

    private void langugesNamessetup() {
        langugesNames = new ArrayList<>();
        langugesNames.add("العربیه");
        langugesNames.add("English");
        langugesNames.add("française");
        langugesNames.add("Deutsch");
        langugesNames.add("한국어");
        langugesNames.add("Italiano");
        langugesNames.add("فارسی");
        langugesNames.add("português");
        langugesNames.add("español");
    }

    private void ChangeLanguge(Context context,String Languge){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("language", Languge);
        editor.apply();
    }

    private void langugesetup() {
        languges = new ArrayList<>();
        languges.add("Arabiac");
        languges.add("English");
        languges.add("French");
        languges.add("German");
        languges.add("Korean");
        languges.add("Italian");
        languges.add("Persian");
        languges.add("Portuguese");
        languges.add("Spanish");

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Languge.this,Entrance.class));
        finish();
    }
}
