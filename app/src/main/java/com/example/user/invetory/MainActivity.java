package com.example.user.invetory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.example.user.invetory.BaseActivitysAndFragmensNeed.Entrance;

import com.example.user.invetory.ForgetPass.ForgetPassBaseActivity;
import com.example.user.invetory.SignUp.SignUpBaseActivity;

import java.io.File;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import com.example.user.invetory.Internet.CheckEmailAddress;
import com.example.user.invetory.BaseActivitysAndFragmensNeed.CreateTable;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MainActivity extends AppCompatActivity {

    TextView new_account;
    TextView forget;
    Button Login;
    EditText password , username;
    Cryptor cryptor = new Cryptor();
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static String KEY = "afsaneh";
    TSnackbar tSnackbar;
    int color;
    private static int RETRYREQUEST = 0;
    CheckEmailAddress check;
    CreateTable table;




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleSSLHandshake();
        preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme",0);


        switch (themepos){
            case 0 : color = ContextCompat.getColor(this,R.color.snblue); setTheme(R.style.DefultTheme); break;
            case 1 : color = ContextCompat.getColor(this,R.color.snred);setTheme(R.style.RedTheme); break;
            case 2 : color = ContextCompat.getColor(this,R.color.sngreen);setTheme(R.style.GreenTheme); break;
            case 3 : color = ContextCompat.getColor(this,R.color.night);setTheme(R.style.NightMode); break;
        }

        setContentView(R.layout.activity_main);

        new_account = (TextView)findViewById(R.id.new_account);
        password = (EditText)findViewById(R.id.password);
        username = (EditText) findViewById(R.id.username);
        Login = (Button)findViewById(R.id.log_in);
        forget = (TextView) findViewById(R.id.forget);

        File[] files = getFilesDir().listFiles();
        if(files != null)
            for(File file : files) {
                file.delete();
            }
        File[] cashfiles = getCacheDir().listFiles();
        if(cashfiles != null)
            for(File file : cashfiles) {
                file.delete();
            }

            check = new CheckEmailAddress();
            table = new CreateTable();


            forget.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    startActivity(new Intent(MainActivity.this,ForgetPassBaseActivity.class));
                }
            });


            Login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    username.setEnabled(false);
                    password.setEnabled(false);
                    tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Please wait...", TSnackbar.LENGTH_INDEFINITE);
                    View view = tSnackbar.getView();
                    view.setBackgroundColor(Color.parseColor("#dedede"));
                    TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(color);
                    tSnackbar.show();

                    if (!username.getText().toString().trim().isEmpty()&&!password.getText().toString().trim().isEmpty()){
                        check.Check_Email(MainActivity.this,username,password,tSnackbar, username.getText().toString(), new CheckEmailAddress.VolleyCallback() {
                            @Override
                            public void onSuccessResponse(final String result) {
                                table.createtable(MainActivity.this,username,password,tSnackbar,username.getText().toString(), new CreateTable.createTableCallBack() {
                                    @Override
                                    public void tableCallBack(String respons) {
                                        if (!result.trim().isEmpty()){

                                            String code = cryptor.Decrypt(result,KEY);
                                            if (code != null && password.getText().toString().equals(code)){
                                                final Intent intent = new Intent(MainActivity.this,Entrance.class);
                                                intent.putExtra("email",username.getText().toString());
                                                editor.putBoolean("getfromserver" , true);
                                                editor.putBoolean("close MainActivity" , false);
                                                editor.apply();
                                                startActivity(intent);
                                                finish();
                                            }else if (code == null){
                                                Toast.makeText(MainActivity.this,"Somthing wrong",Toast.LENGTH_SHORT).show();
                                                username.setEnabled(true);
                                                password.setEnabled(true);
                                                tSnackbar.dismiss();
                                            }else {
                                                Toast.makeText(MainActivity.this, "wrong password", Toast.LENGTH_SHORT).show();
                                                username.setEnabled(true);
                                                password.setEnabled(true);
                                                tSnackbar.dismiss();
                                            }
                                        }else {
                                            Toast.makeText(MainActivity.this, "Email is incorrect", Toast.LENGTH_SHORT).show();
                                            username.setEnabled(true);
                                            password.setEnabled(true);
                                            tSnackbar.dismiss();
                                        }
                                    }
                                });
                            }
                        });
                    }else{
                        username.setEnabled(true);
                        password.setEnabled(true);
                        Toast.makeText(MainActivity.this, "Please fill All fild", Toast.LENGTH_SHORT).show();
                        tSnackbar.dismiss();
                    }
                }
            });

        new_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this , SignUpBaseActivity.class);
                startActivity(intent);
            }
        });
    }
    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}