package com.example.user.invetory;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.invetory.DetailePage.DetailActivity;
import com.example.user.invetory.sell.SellActivity;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.github.rubensousa.bottomsheetbuilder.adapter.BottomSheetItemClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Cache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    List<ContentObject> objects ;
    Context context;
    String email;
    Picasso picasso;
    LruCache<Integer,Bitmap> imagecach;
    RequestOptions requestOptions;
    ImageRequest imgRequest;
    RequestQueue queue;
    int color;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;





    public MyAdapter(Context context , ArrayList objects , String email){


        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme",0);



        switch (themepos){
            case 0 :
                color = ContextCompat.getColor(context,R.color.snblue);
                break;
            case 1 :
                color = ContextCompat.getColor(context,R.color.snred);
                break;
            case 2 :
                color = ContextCompat.getColor(context,R.color.sngreen);
                break;
            case 3 :
                color = ContextCompat.getColor(context,R.color.Tab_background);
                break;
        }

        this.objects = objects;
        this.context = context;
        this.email = email;
        picasso = Picasso.get();
        int maxsize = (int) Runtime.getRuntime().maxMemory();
        imagecach = new LruCache<>(maxsize / 8);
        requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);


    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView name,stock,barcode ;
        ImageView img;
        public RelativeLayout forground;
        AppCompatImageButton sell;


        public ViewHolder(View Itemview){
            super(Itemview);
            name = itemView.findViewById(R.id.name);
            sell = itemView.findViewById(R.id.sell);
            stock = itemView.findViewById(R.id.stock);
            barcode = itemView.findViewById(R.id.barcode);
            img = itemView.findViewById(R.id.img);
            forground = itemView.findViewById(R.id.view_forground);
            sell.getDrawable().setColorFilter(color,PorterDuff.Mode.SRC_IN);

        }
    }


    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

            holder.name.setText(String.valueOf(objects.get(position).getName()));
            holder.stock.setText(objects.get(position).getStock()+objects.get(position).getStockFormat());
            holder.barcode.setText(objects.get(position).getBarCode());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                picasso.load("http://www.inventory-customer.com/"+objects.get(position).getImageAddress())//
                        .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(holder.img, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imgRequest = new ImageRequest("http://www.inventory-customer.com/"+objects.get(position).getImageAddress(), new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                holder.img.setImageBitmap(response);
                            }
                        }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                        queue.add(imgRequest);
                                    }
                                });
                        queue = Volley.newRequestQueue(context);
                        imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queue.add(imgRequest);

                    }
                });
            }else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    Glide.with(context).load("http://www.inventory-customer.com/" + objects.get(position).getImageAddress()).apply(requestOptions).into(holder.img );

            }

                holder.sell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, SellActivity.class);
                        intent.putExtra("stock",objects.get(position).getStock());
                        intent.putExtra("stockformat",objects.get(position).getStockFormat());
                        intent.putExtra("sale",objects.get(position).getSale_Price());
                        intent.putExtra("saleformat",objects.get(position).getSaleFormat());
                        intent.putExtra("name",objects.get(position).getName());
                        intent.putExtra("id",objects.get(position).getID());
                        context.startActivity(intent);


                    }
                });

            holder.forground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,DetailActivity.class);
                    intent.putExtra("id",objects.get(position).getID());
                    context.startActivity(intent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }



}
