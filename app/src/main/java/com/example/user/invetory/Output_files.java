package com.example.user.invetory;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.content.Context.MODE_PRIVATE;

public class Output_files {


    private static Context context ;

    public Output_files(Context con ){
        this.context = con ;
    }

    //-------------------image----------------------------

    public void write_prof_iamge(String encoded) {
        byte[] bitmapbytes = Base64.decode(encoded,Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapbytes,0,bitmapbytes.length);
        try {
            FileOutputStream fos = context.openFileOutput("profileimage.png" , MODE_PRIVATE);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG ,100 , baos);
            byte[] arraybitmap = baos.toByteArray();
            fos.write(arraybitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void writecurrentprofile(Bitmap bitmap){
        try {
            FileOutputStream stream = context.openFileOutput("profile.png",MODE_PRIVATE);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,bos);
            stream.write(bos.toByteArray());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //------------------email-----------------------------
    public void write(String filename , String content){
        try {
            FileOutputStream fos = context.openFileOutput(filename , MODE_PRIVATE);
            byte [] arrystring = content.getBytes();
            fos.write(arrystring);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String read(String filename){

        try {
            FileInputStream fis = context.openFileInput(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null){
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    //-----------------name-lastname-----------------------
    public void write_nameandlastname(String filename , String content){
        try {
            FileOutputStream fos = context.openFileOutput(filename , MODE_PRIVATE);
            byte [] arrystring = content.getBytes();
            fos.write(arrystring);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String read_nameandlastname (String filename){

        try {
            FileInputStream fis = context.openFileInput(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null){
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    //----------------pass------------------------
    public void write_pass(String filename , String content){
        try {
            FileOutputStream fos = context.openFileOutput(filename , MODE_PRIVATE);
            byte [] arrystring = content.getBytes();
            fos.write(arrystring);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String read_pass (String filename){

        try {
            FileInputStream fis = context.openFileInput(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null){
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
