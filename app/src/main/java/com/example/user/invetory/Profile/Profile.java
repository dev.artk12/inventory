package com.example.user.invetory.Profile;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.example.user.invetory.BaseActivitysAndFragmensNeed.Entrance;

import com.example.user.invetory.Cryptor;
import com.example.user.invetory.LocalHelper;
import com.example.user.invetory.MyContextWrapper;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;
import com.example.user.invetory.Ucrop;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.hdodenhof.circleimageview.CircleImageView;


public class Profile extends AppCompatActivity {

    DisplayMetrics metrics;
    Button save_btn , gallery , camera;
    TextView emailtv;
    Output_files files;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Uri uri;
    int CAPTURE_IMAGE_CODE = 1;
    int GALLERY_IMAGE_CODE = 2;
    CircleImageView img;
    Bitmap bitmap;
    View view;
    EditText name , confrimpassword;
    RelativeLayout parentview;
    ImageButton next , back;
    TextView password,profile;
    int TAG;
    Cryptor cryptor;
    RequestQueue queue;
    int color;

    private static String ENCRYPT_KEY = "afsaneh";


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        preferences = PreferenceManager.getDefaultSharedPreferences(Profile.this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme", 0);

        switch (themepos){
            case 0 : color = ContextCompat.getColor(this,R.color.snblue); setTheme(R.style.DefultTheme); break;
            case 1 : color = ContextCompat.getColor(this,R.color.snred);setTheme(R.style.RedTheme); break;
            case 2 : color = ContextCompat.getColor(this,R.color.sngreen);setTheme(R.style.GreenTheme); break;
            case 3 : color = ContextCompat.getColor(this,R.color.night);setTheme(R.style.NightMode); break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        view = (View) findViewById(R.id.view);
        img = (CircleImageView) findViewById(R.id.circleImageView);
        emailtv = (TextView) findViewById(R.id.email);
        name = (EditText) findViewById(R.id.name);
        confrimpassword = (EditText) findViewById(R.id.confrimpassword);
        save_btn = (Button) findViewById(R.id.save);
        camera = (Button) findViewById(R.id.camera);
        gallery = (Button) findViewById(R.id.gallery);
        parentview = (RelativeLayout) findViewById(R.id.parentview);
        next = (ImageButton) findViewById(R.id.next);
        back = (ImageButton) findViewById(R.id.back);
        profile = (TextView)findViewById(R.id.profiletext);
        password = (TextView)findViewById(R.id.password);
        cryptor = new Cryptor();
        profile.setVisibility(View.VISIBLE);
        back.setVisibility(View.GONE);
        password.setVisibility(View.GONE);
        name.setHint(R.string.Name);
        TAG = 1;
        queue = Volley.newRequestQueue(this);

        confrimpassword.setVisibility(View.GONE);
        handleSSLHandshake();



        //view
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float density  = getResources().getDisplayMetrics().density;
        int height_color =  metrics.heightPixels/3;
        view.getLayoutParams().height = height_color;
        parentview.getLayoutParams().height = height_color;

        //image
        img.getLayoutParams().height = metrics.heightPixels/6;
        img.getLayoutParams().width = metrics.widthPixels/5;

        RelativeLayout.LayoutParams imageParams = (RelativeLayout.LayoutParams) img.getLayoutParams();
        int marginleft = metrics.widthPixels/3;
        float pxtimagewidth = 50 * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        float pxtimageheight = img.getLayoutParams().height/2 ;
        //Toast.makeText(this, ""+img.getLayoutParams().height, Toast.LENGTH_SHORT).show();
        imageParams.setMargins((int) (marginleft - pxtimagewidth), (int) (height_color-pxtimageheight),0,0);
        img.setLayoutParams(imageParams);

        //name
        final RelativeLayout.LayoutParams nameParams = (RelativeLayout.LayoutParams) name.getLayoutParams();
        nameParams.setMargins(0,height_color/2,0,0);
        name.setLayoutParams(nameParams);

        //lastname
        final RelativeLayout.LayoutParams lastnameParams = (RelativeLayout.LayoutParams) confrimpassword.getLayoutParams();
        lastnameParams.setMargins(0,height_color,0,0);
        confrimpassword.setLayoutParams(lastnameParams);

        //save
        RelativeLayout.LayoutParams saveParams = (RelativeLayout.LayoutParams) save_btn.getLayoutParams();
        saveParams.setMargins(0,0,0,height_color/4);
        save_btn.setLayoutParams(saveParams);


        //camera
        final RelativeLayout.LayoutParams cameraParams = (RelativeLayout.LayoutParams) camera.getLayoutParams();
        //float pxtleftcamera= img.getLayoutParams().width/2 * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        cameraParams .setMargins(0,0, (int) (img.getLayoutParams().width/1.3),0);
        camera.setLayoutParams(cameraParams);

        //gallery
        RelativeLayout.LayoutParams galleryParams = (RelativeLayout.LayoutParams) gallery.getLayoutParams();
        //float pxtleftcamera= img.getLayoutParams().width/2 * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        galleryParams.setMargins(0,0, (int) (img.getLayoutParams().width/1.3),0);
        gallery.setLayoutParams(galleryParams);

        //----------------------------------------------------------------

        files = new Output_files(Profile.this);

        final String nametext = files.read_nameandlastname("name");//
        final String emailtext = files.read("email");//;

        name.setText(nametext);
        emailtv.setText(emailtext);


        File currentfile = getFilesDir();
        bitmap = BitmapFactory.decodeFile(currentfile.getAbsolutePath()+"/profileimage.png");
        img.setImageBitmap(bitmap);

        camera.setVisibility(View.GONE);
        gallery.setVisibility(View.GONE);
        camera.setEnabled(false);
        gallery.setEnabled(false);

        name.setInputType(InputType.TYPE_CLASS_TEXT);
        confrimpassword.setInputType(InputType.TYPE_CLASS_TEXT);
        name.setMaxLines(1);
        confrimpassword.setMaxLines(1);


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gallery.getVisibility() == View.GONE || camera.getVisibility() == View.GONE){
                    Animation animation = AnimationUtils.loadAnimation(Profile.this,R.anim.camera_profile_button_in);
                    camera.setAnimation(animation);
                    Animation animation2 = AnimationUtils.loadAnimation(Profile.this,R.anim.gallery_profile_button_in);
                    gallery.setAnimation(animation2);
                    camera.setVisibility(View.VISIBLE);
                    gallery.setVisibility(View.VISIBLE);
                    camera.setEnabled(true);
                    gallery.setEnabled(true);
                }else if (camera.getVisibility() == View.VISIBLE || gallery.getVisibility() == View.VISIBLE){
                    Animation animation = AnimationUtils.loadAnimation(Profile.this,R.anim.camera_profile_button_out);
                    camera.setAnimation(animation);
                    Animation animation2 = AnimationUtils.loadAnimation(Profile.this,R.anim.gallery_profile_button_out);
                    gallery.setAnimation(animation2);
                    camera.setVisibility(View.GONE);
                    gallery.setVisibility(View.GONE);
                    camera.setEnabled(false);
                    gallery.setEnabled(false);
                }
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = new File(Environment.getExternalStorageDirectory(),
                        "file"+String.valueOf(System.currentTimeMillis())+".jpg");
                uri = Uri.fromFile(file);
                CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
                CamIntent.putExtra("return-data",true);
                startActivityForResult(CamIntent,CAPTURE_IMAGE_CODE);
            }
        });


        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ucrop.openGalletry(Profile.this,GALLERY_IMAGE_CODE);
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NextOptions();

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackOptions(nametext);
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                save_btn.setEnabled(false);


                if (TAG == 1 ){
                    final String EncryptionName = cryptor.Encrypt(name.getText().toString(),ENCRYPT_KEY);
                    name.setEnabled(false);
                    BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
                    Bitmap currentbitmap = drawable.getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    currentbitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                    String result = Base64.encodeToString(stream.toByteArray(),Base64.DEFAULT);
                    final String EncryptionImage = cryptor.Encrypt(result,ENCRYPT_KEY);

                    nameAndlastname(name,save_btn,EncryptionName,EncryptionImage);
                }else if (TAG == 2){
                    if (name.getText().length() >= 8 && confrimpassword.getText().length() >= 8 ){

                        if (name.getText().toString().equals(confrimpassword.getText().toString())){
                            String uri = "http://www.inventory-customer.com/source/passwordchange.php";

                            name.setEnabled(false);
                            confrimpassword.setEnabled(false);
                            TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Working on it...", TSnackbar.LENGTH_INDEFINITE);
                            View view = tSnackbar.getView();
                            view.setBackgroundColor(Color.parseColor("#dedede"));
                            TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                            textView.setTextColor(color);
                            tSnackbar.show();


                            StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Toast.makeText(Profile.this, response, Toast.LENGTH_SHORT).show();

                                    editor.putBoolean("getfromserver" , true);
                                    editor.apply();
                                    Intent intent = new Intent(Profile.this,Entrance.class);
                                    intent.putExtra("email",emailtv.getText().toString());
                                    TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Done", TSnackbar.LENGTH_SHORT);
                                    View view = tSnackbar.getView();
                                    view.setBackgroundColor(Color.parseColor("#dedede"));
                                    TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                                    textView.setTextColor(color);
                                    tSnackbar.show();

                                    startActivity(intent);
                                    finish();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    Toast.makeText(Profile.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                                    TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Somthing wrong...", TSnackbar.LENGTH_SHORT);
                                    View view = tSnackbar.getView();
                                    view.setBackgroundColor(Color.parseColor("#dedede"));
                                    TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                                    textView.setTextColor(color);
                                    tSnackbar.show();
                                    name.setEnabled(true);
                                    confrimpassword.setEnabled(true);

                                    save_btn.setEnabled(true);


                                }
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String , String> params = new HashMap<>();
                                    params.put("email" , emailtv.getText().toString());
                                    String password = cryptor.Encrypt(name.getText().toString(),ENCRYPT_KEY);
                                    params.put("password",password);
                                    return params;
                                }
                            };
                            request.setShouldCache(false);
                            request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queue.add(request);

                        }else{
                            Toast.makeText(Profile.this, "it is different", Toast.LENGTH_SHORT).show();
                            save_btn.setEnabled(true);
                        }
                    }else if (name.getText().length() < 8 || confrimpassword.getText().length() < 8 ){
                        Toast.makeText(Profile.this, "password must be 8 charecters", Toast.LENGTH_SHORT).show();
                        save_btn.setEnabled(true);
                    }
                }

            }
        });

    }

    private void NextOptions() {
        Animation animation1 = AnimationUtils.loadAnimation(Profile.this, R.anim.prof_change_out);
        profile.setAnimation(animation1);
        profile.setVisibility(View.GONE);
        next.setVisibility(View.GONE);
        Animation animation2 = AnimationUtils.loadAnimation(Profile.this,R.anim.password_change_in);
        password.setAnimation(animation2);
        password.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);
        name.getText().clear();
        name.setHint(R.string.password);
        confrimpassword.setHint(R.string.confrim_password);
        name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        confrimpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        name.setMaxLines(1);
        confrimpassword.setMaxLines(1);
        TAG = 2;
        profile.setEnabled(false);
        confrimpassword.setVisibility(View.VISIBLE);
    }

    private void BackOptions(String nametext) {
        Animation animation1 = AnimationUtils.loadAnimation(Profile.this, R.anim.prof_change_in);
        profile.setAnimation(animation1);
        profile.setVisibility(View.VISIBLE);
        next.setVisibility(View.VISIBLE);
        Animation animation2 = AnimationUtils.loadAnimation(Profile.this,R.anim.password_change_out);
        password.setAnimation(animation2);
        password.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
        name.setText(nametext);
        name.setHint(R.string.Name);
        name.setInputType(InputType.TYPE_CLASS_TEXT);
        name.setMaxLines(1);
        TAG = 1;
        confrimpassword.setVisibility(View.GONE);
    }

    private void nameAndlastname(final EditText name,final Button btn, final String EncryptionName, final String EncryptionImage) {


        if (name.getText().toString().length() > 2  ){
            String uri = "http://www.inventory-customer.com/source/UpdateUserDetails.php";

            TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Working on it...", TSnackbar.LENGTH_INDEFINITE);
            View view = tSnackbar.getView();
            view.setBackgroundColor(Color.parseColor("#dedede"));
            TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
            textView.setTextColor(color);
            tSnackbar.show();
            name.setEnabled(false);


            StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(Profile.this, response, Toast.LENGTH_SHORT).show();
                    editor.putBoolean("getfromserver" , true);
                    editor.apply();
                    Intent intent = new Intent(Profile.this,Entrance.class);
                    intent.putExtra("email",emailtv.getText().toString());
                    TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Done", TSnackbar.LENGTH_SHORT);
                    View view = tSnackbar.getView();
                    view.setBackgroundColor(Color.parseColor("#dedede"));
                    TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(color);
                    tSnackbar.show();

                    startActivity(intent);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    btn.setEnabled(true);
                    TSnackbar tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Somthing wrong...", TSnackbar.LENGTH_SHORT);
                    View view = tSnackbar.getView();
                    view.setBackgroundColor(Color.parseColor("#dedede"));
                    TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(color);
                    tSnackbar.show();
                    name.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String , String> params = new HashMap<>();
                    params.put("email" , emailtv.getText().toString());
                    params.put("name",EncryptionName);
                    params.put("photo" , EncryptionImage);

                    return params;
                }
            };
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(15),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        }else if (name.getText().length() <= 2  ){
            Toast.makeText(Profile.this, "name and last name must be 3 charecters", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Ucrop ucrop = new Ucrop(color);
        if (requestCode == CAPTURE_IMAGE_CODE && resultCode == RESULT_OK) {
            ucrop.startCropActivity(Profile.this,uri,"profile");
        } else if (requestCode == GALLERY_IMAGE_CODE && resultCode == RESULT_OK) {
            uri = data.getData();
            ucrop.startCropActivity(Profile.this,uri,"profile");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("image",true);
            editor.apply();
            Bitmap bitmap = Ucrop.handleCropResult(this,data);
            if (bitmap != null){
                img.setImageBitmap(bitmap);
            }
            gallery.setVisibility(View.GONE);
            camera.setVisibility(View.GONE);
        }else if ((requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED)){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this,Entrance.class));
        super.onBackPressed();
    }
    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
