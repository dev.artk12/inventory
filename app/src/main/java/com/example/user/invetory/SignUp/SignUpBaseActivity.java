package com.example.user.invetory.SignUp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Base64;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.LocalHelper;
import com.example.user.invetory.MyContextWrapper;
import com.example.user.invetory.Profile.Profile;
import com.example.user.invetory.R;
import com.example.user.invetory.Ucrop;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpBaseActivity extends AppCompatActivity implements SignupCallBackTab1,SignupCallBackTab2 {

    private static final int REQUEST_IMAGE = 1 ;
    private static final int REQUEST_PERMISSION_CAMERA = 101 ;
    private static final int REQUEST_OPEN_GALLETY = 2;
    private static final int REQUEST_PERMISSION_GALLRY = 102 ;

    public static CustomViewPager_SignUp viewPager;
    MyAdapater adapater;
    Map<String,String> values;
    Bitmap bitmap;

    private static String NAME_KEY = "Name";
    private static String EMAIL_KEY = "Email";
    private static String PASSWORD_KEY = "Password";
    private static String CAMERA_GALLERY_BUTTONS = "buttons";
    private static String NEXTBUTTON = "nextbtn";
    private static String CAMERA_BUTTON = "camera";
    private static String GALLERY_BUTTON = "gallery";
    private static String PROFILE_IMAGEVIEW = "img";
    private static String ENCRYPT_KEY = "afsaneh";

    private Uri uri;
    ByteArrayOutputStream stream;
    String[] permissions = {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.INTERNET};
    Map<String,View> viewMap;
    Animation signupProfileOpen , buttonsOpen, buttonsClose, signupProfileClose;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Cryptor cryptor;
    RequestQueue queue;
    SweetAlertDialog dialog;
    StringRequest request;
    int color;
    TSnackbar tSnackbar;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelper.onAttach(newBase,"en"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleSSLHandshake();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme", 0);

        switch (themepos) {
            case 0:
                color = ContextCompat.getColor(this,R.color.snblue);
                setTheme(R.style.DefultTheme);
                break;
            case 1:
                color = ContextCompat.getColor(this,R.color.snred);
                setTheme(R.style.RedTheme);
                break;
            case 2:
                color = ContextCompat.getColor(this,R.color.sngreen);
                setTheme(R.style.GreenTheme);
                break;
            case 3:
                color = ContextCompat.getColor(this,R.color.night);
                setTheme(R.style.NightMode);
        }

        setContentView(R.layout.activity_signup);
        values = new HashMap<>();
        viewMap = new HashMap<>();
        cryptor = new Cryptor();
        queue = Volley.newRequestQueue(this);

        viewPager = (CustomViewPager_SignUp) findViewById(R.id.scrollView);
        adapater = new MyAdapater(getSupportFragmentManager());
        viewPager.setAdapter(adapater);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        signupProfileOpen = AnimationUtils.loadAnimation(this,R.anim.signup_profile_1);
        buttonsOpen = AnimationUtils.loadAnimation(this,R.anim.buttonssignupopen);
        buttonsClose = AnimationUtils.loadAnimation(this,R.anim.buttonssignupclose);
        signupProfileClose = AnimationUtils.loadAnimation(this,R.anim.signup_profile_2);


    }

    @Override
    public void valuesCallbackTab1(String email, String name) {
        values.put(NAME_KEY,name);
        values.put(EMAIL_KEY,email);
    }

    @Override
    public void ParentButtons(LinearLayoutCompat buttons) {
        viewMap.put(CAMERA_GALLERY_BUTTONS,buttons);
    }

    @Override
    public void NextButtonCallBack(AppCompatButton button) {
        viewMap.put(NEXTBUTTON,button);

        if (Sign_UP_Tab1.emailbool && Sign_UP_Tab1.namebool && bitmap != null){
             button.setEnabled(true);
             viewPager.setunLuck(true);
        }else {
             button.setEnabled(false);
             SignUpBaseActivity.viewPager.setunLuck(false);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
            }
        });
    }


    @Override
    public void CameraButton(AppCompatButton camera) {
            viewMap.put(CAMERA_BUTTON,camera);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean camrabool = checkPermissionCamera();
                boolean ExternalStorage = checkPermissionExternalStorage();

                if (camrabool && ExternalStorage){
                    CameraRequest();
                }else {
                    ActivityCompat.requestPermissions(SignUpBaseActivity.this,permissions, REQUEST_PERMISSION_CAMERA);
                }
            }
        });

    }

    @Override
    public void ImageViewProfile(CircleImageView img) {
        viewMap.put(PROFILE_IMAGEVIEW,img);
    }

    @Override
    public void GalleryButton(AppCompatButton gallery) {
        viewMap.put(GALLERY_BUTTON,gallery);

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean camrabool = checkPermissionCamera();
                boolean ExternalStorage = checkPermissionExternalStorage();

                if (camrabool && ExternalStorage){
                    Ucrop.openGalletry(SignUpBaseActivity.this,REQUEST_OPEN_GALLETY);
                }else {
                    ActivityCompat.requestPermissions(SignUpBaseActivity.this,permissions, REQUEST_PERMISSION_GALLRY);
                }
            }
        });

    }

    @Override
    public void valuesCallbackTab2(String password) {
        values.put(PASSWORD_KEY,password);
    }

    @Override
    public void finishButton(final AppCompatButton finish) {

        stream = new ByteArrayOutputStream();

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new SweetAlertDialog(SignUpBaseActivity.this,SweetAlertDialog.PROGRESS_TYPE);
                dialog.setTitleText("Please wait");
                dialog.setCancelable(false);
                dialog.show();
                final String Name = values.get(NAME_KEY);
                final String Email = values.get(EMAIL_KEY);
                final String Password = values.get(PASSWORD_KEY);
                bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                byte[] imagebytes = stream.toByteArray();
                String stringimage = Base64.encodeToString(imagebytes,Base64.DEFAULT);
                final String PasswordEncrypt = cryptor.Encrypt(Password,ENCRYPT_KEY);
                final String NameEncrypt = cryptor.Encrypt(Name,ENCRYPT_KEY);
                final String imageEncrypt = cryptor.Encrypt(stringimage,ENCRYPT_KEY);
                handleSSLHandshake();

                WriteUserRequest(Email, PasswordEncrypt, NameEncrypt, imageEncrypt, new write() {
                    @Override
                    public void onsusess(final String responseemail) {
                        //Toast.makeText(SignUpBaseActivity.this, ""+responseemail.trim(), Toast.LENGTH_LONG).show();
                        if (responseemail.trim().equals("Email Found")){
                            dialog.dismissWithAnimation();
                            dialog = new SweetAlertDialog(SignUpBaseActivity.this);
                            dialog.setTitleText("Email is found you can sign in");
                            dialog.setCancelable(false);
                            dialog.show();
                        }else {
                            CrateTableRequest(Email, new createtableinterface() {
                                @Override
                                public void onsusess(String response) {
                                    dialog.dismissWithAnimation();
                                    finish();
                                }
                            });
                        }
                    }
                });
            }
        });

    }

    private void WriteUserRequest(final String email, final String passwordEncrypt, final String nameEncrypt, final String imageEncrypt, final write callback) {
        request = new StringRequest(Request.Method.POST, "http://www.inventory-customer.com/source/users.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                callback.onsusess(response);
            }
        }, new Response.ErrorListener()  {
            @Override
            public void onErrorResponse(VolleyError error) {
                tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"Contection error please try again...", TSnackbar.LENGTH_INDEFINITE);
                View view = tSnackbar.getView();
                view.setBackgroundColor(Color.parseColor("#dedede"));
                TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                textView.setTextColor(color);

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("EmailAddress", email);
                map.put("Name", nameEncrypt);
                map.put("Password", passwordEncrypt);
                map.put("Photo",imageEncrypt);

                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(15),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    private void CrateTableRequest(final String email, final createtableinterface callback) {
        StringRequest creattable = new StringRequest(Request.Method.POST, "http://www.inventory-customer.com/source/valuetable.php",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onsusess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismissWithAnimation();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("EmailAddress", email);
                return map;
            }
        };
        creattable.setShouldCache(false);
        creattable.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(creattable);
    }

    public class MyAdapater extends FragmentPagerAdapter{

        public MyAdapater(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0 : return new Sign_UP_Tab1();
                case 1 : return new Sign_UP_Tab2();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            CameraRequest();
        }if (requestCode == REQUEST_PERMISSION_GALLRY && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Ucrop.openGalletry(SignUpBaseActivity.this,REQUEST_OPEN_GALLETY);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Ucrop ucrop = new Ucrop(color);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            ucrop.startCropActivity(SignUpBaseActivity.this,uri,"profile");
        }else if (requestCode == REQUEST_OPEN_GALLETY && resultCode == RESULT_OK){
            uri = data.getData();
            ucrop.startCropActivity(SignUpBaseActivity.this,uri,"profile");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            bitmap = Ucrop.handleCropResult(SignUpBaseActivity.this,data);
            CircleImageView profile = (CircleImageView) viewMap.get(PROFILE_IMAGEVIEW);
            profile.setImageBitmap(bitmap);
            ucropSetup(profile);

            if (Sign_UP_Tab1.emailbool && Sign_UP_Tab1.namebool){
                AppCompatButton next = (AppCompatButton) viewMap.get(NEXTBUTTON);
                next.setEnabled(true);
                viewPager.setunLuck(true);
            }
        }else if(requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void ucropSetup(CircleImageView profile) {
        AppCompatButton gallery = (AppCompatButton) viewMap.get(GALLERY_BUTTON);
        AppCompatButton camera = (AppCompatButton) viewMap.get(CAMERA_BUTTON);
        LinearLayoutCompat parent = (LinearLayoutCompat) viewMap.get(CAMERA_GALLERY_BUTTONS);
        gallery.setEnabled(false);
        camera.setEnabled(false);
        profile.startAnimation(signupProfileClose);
        parent.startAnimation(buttonsClose);
        parent.setVisibility(View.INVISIBLE);
    }

    private void CameraRequest() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/inventory/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        startActivityForResult(CamIntent,REQUEST_IMAGE);
    }

    boolean checkPermissionCamera(){
        int res = checkCallingOrSelfPermission(permissions[0]);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    boolean checkPermissionExternalStorage(){
        int res = checkCallingOrSelfPermission(permissions[1]);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    interface createtableinterface{
        void onsusess(String response);
    }

    interface write{
        void onsusess(String response);
    }

}
