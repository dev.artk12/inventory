package com.example.user.invetory.SignUp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.user.invetory.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class Sign_UP_Tab1 extends Fragment {


    CircleImageView profile;
    AppCompatEditText email,companyname;
    AppCompatImageButton emailcheck,companynamecheck;
    AppCompatButton nextbtn , gallery , camera;
    int colorwrong , greencolor;
    public static boolean emailbool,namebool;
    SignupCallBackTab1 callBackTab;
    Animation signupProfileOpen , buttonsOpen, buttonsClose, signupProfileClose;
    LinearLayoutCompat buttons;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBackTab = (SignupCallBackTab1) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign__up__tab1, container, false);
        profile = (CircleImageView) view.findViewById(R.id.profile);
        email = (AppCompatEditText) view.findViewById(R.id.email);
        companyname = (AppCompatEditText) view.findViewById(R.id.company_name);
        emailcheck = (AppCompatImageButton) view.findViewById(R.id.emailcheck);
        companynamecheck = (AppCompatImageButton) view.findViewById(R.id.company_namecheck);
        nextbtn = (AppCompatButton) view.findViewById(R.id.nextbtn);
        buttons = (LinearLayoutCompat) view.findViewById(R.id.buttons);
        gallery = (AppCompatButton) view.findViewById(R.id.gallery);
        camera = (AppCompatButton) view.findViewById(R.id.camera);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();

        callBackTab.CameraButton(camera);
        callBackTab.ImageViewProfile(profile);
        callBackTab.GalleryButton(gallery);
        callBackTab.ParentButtons(buttons);

        signupProfileOpen = AnimationUtils.loadAnimation(getContext(),R.anim.signup_profile_1);
        buttonsOpen = AnimationUtils.loadAnimation(getContext(),R.anim.buttonssignupopen);
        buttonsClose = AnimationUtils.loadAnimation(getContext(),R.anim.buttonssignupclose);
        signupProfileClose = AnimationUtils.loadAnimation(getContext(),R.anim.signup_profile_2);


        colorwrong = ContextCompat.getColor(getContext(),R.color.wrongcheckedtsignup);
        greencolor = ContextCompat.getColor(getContext(),R.color.greencheckedtsignup);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttons.getVisibility() == View.INVISIBLE){
                    setVisible();
                }else {
                    setInvisible();
                }

            }
        });

        namebool = false;
        emailbool = false;

        nextbtn.setEnabled(false);



        emailcheck.getDrawable().setColorFilter(colorwrong, PorterDuff.Mode.SRC_IN);
        companynamecheck.getDrawable().setColorFilter(colorwrong,PorterDuff.Mode.SRC_IN);

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()){
                    emailcheck.getDrawable().setColorFilter(greencolor, PorterDuff.Mode.SRC_IN);
                    emailbool = true;
                }else{
                    emailcheck.getDrawable().setColorFilter(colorwrong, PorterDuff.Mode.SRC_IN);
                    emailbool = false;
                }
                check();
                callBackTab.valuesCallbackTab1(email.getText().toString(),companyname.getText().toString());
            }
        });
        companyname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 3){
                    companynamecheck.getDrawable().setColorFilter(greencolor, PorterDuff.Mode.SRC_IN);
                    namebool = true;
                }else{
                    companynamecheck.getDrawable().setColorFilter(colorwrong, PorterDuff.Mode.SRC_IN);
                    namebool = false;
                }
                check();
                callBackTab.valuesCallbackTab1(email.getText().toString(),companyname.getText().toString());

            }
        });

        return view ;
    }

    private void setInvisible() {
        profile.startAnimation(signupProfileClose);
        buttons.startAnimation(buttonsClose);
        camera.setEnabled(false);
        gallery.setEnabled(false);
        buttons.setVisibility(View.INVISIBLE);
    }

    private void setVisible() {
        profile.startAnimation(signupProfileOpen);
        buttons.startAnimation(buttonsOpen);
        camera.setEnabled(true);
        gallery.setEnabled(true);
        buttons.setVisibility(View.VISIBLE);
    }

    private void check() {
        if (emailbool && namebool){
           // nextbtn.setEnabled(true);
           // MainActivity.viewPager.setunLuck(true);
            editor.putBoolean("Texts",true).apply();
            callBackTab.NextButtonCallBack(nextbtn);

        }else{
           // nextbtn.setEnabled(false);
           // MainActivity.viewPager.setunLuck(false);
            editor.putBoolean("Texts",false).apply();
            callBackTab.NextButtonCallBack(nextbtn);
        }

    }

}
