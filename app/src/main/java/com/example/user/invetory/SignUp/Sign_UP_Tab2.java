package com.example.user.invetory.SignUp;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.invetory.R;


public class Sign_UP_Tab2 extends Fragment implements TextWatcher {

    AppCompatEditText password,confrimpassword;
    int wrongcolor, greencolor;
    AppCompatImageButton check;
    AppCompatButton finish;
    public static boolean passwordbool;
    SignupCallBackTab2 callBack;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (SignupCallBackTab2) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign__up__tab2, container, false);
        password = (AppCompatEditText) view.findViewById(R.id.password);
        confrimpassword = (AppCompatEditText) view.findViewById(R.id.confrimpassword);
        finish = (AppCompatButton) view.findViewById(R.id.finish);
        check =(AppCompatImageButton) view.findViewById(R.id.passwordcheck);

        wrongcolor = ContextCompat.getColor(getContext(),R.color.wrongcheckedtsignup);
        greencolor = ContextCompat.getColor(getContext(),R.color.greencheckedtsignup);

        check.getDrawable().setColorFilter(wrongcolor, PorterDuff.Mode.SRC_IN);

        password.addTextChangedListener(this);
        confrimpassword.addTextChangedListener(this);

        callBack.finishButton(finish);


        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
       check(password.getText().toString(),confrimpassword.getText().toString());
    }

    private void check(String password, String confrimpassword) {

        if (!password.isEmpty()&&!confrimpassword.isEmpty()&&confrimpassword.equals(password)){
            if (password.length() >7 && confrimpassword.length() >7){
                finish.setEnabled(true);
                check.getDrawable().setColorFilter(greencolor, PorterDuff.Mode.SRC_IN);
                passwordbool = true;
                callBack.valuesCallbackTab2(password);
            }
        } else{
            finish.setEnabled(false);
            check.getDrawable().setColorFilter(wrongcolor, PorterDuff.Mode.SRC_IN);
            passwordbool = false;
        }

    }

}
