package com.example.user.invetory.SignUp;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutCompat;

import de.hdodenhof.circleimageview.CircleImageView;

public interface SignupCallBackTab1 {
    void NextButtonCallBack(AppCompatButton button);
    void CameraButton(AppCompatButton camera);
    void ImageViewProfile(CircleImageView img);
    void GalleryButton(AppCompatButton gallery);
    void valuesCallbackTab1(String email, String name);
    void ParentButtons(LinearLayoutCompat buttons);
}
