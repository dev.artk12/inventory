package com.example.user.invetory;

public class TransAction {

    private  String id;
    private  String Price;
    private  String PeymentMethod;
    private  String BuyOrSell;
    private  String ObjectId;
    private  String TransActionsSerial;
    private  String SaleAmount;

    public  String getId() {
        return id;
    }

    public  void setId(String id) {
        this.id = id;
    }

    public  String getPrice() {
        return Price;
    }

    public  void setPrice(String price) {
        Price = price;
    }

    public  String getPeymentMethod() {
        return PeymentMethod;
    }

    public  void setPeymentMethod(String peymentMethod) {
        PeymentMethod = peymentMethod;
    }

    public  String getBuyOrSell() {
        return BuyOrSell;
    }

    public  void setBuyOrSell(String buyOrSell) {
        BuyOrSell = buyOrSell;
    }

    public  String getObjectId() {
        return ObjectId;
    }

    public  void setObjectId(String objectId) {
        ObjectId = objectId;
    }

    public  String getTransActionsSerial() {
        return TransActionsSerial;
    }

    public  void setTransActionsSerial(String transActionsSerial) {
        TransActionsSerial = transActionsSerial;
    }

    public String getSaleAmount() {
        return SaleAmount;
    }

    public  void setSaleAmount(String saleAmount) {
        SaleAmount = saleAmount;
    }
}
