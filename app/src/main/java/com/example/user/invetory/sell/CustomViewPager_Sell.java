package com.example.user.invetory.sell;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager_Sell extends ViewPager {

    boolean unluck ;


    public CustomViewPager_Sell(@NonNull Context context) {
        super(context);
    }

    public CustomViewPager_Sell(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (unluck == false){
            return false;
        }else
            return super.onTouchEvent(ev);
    }

    @Override
    public void scrollTo(int x, int y) {
        if (unluck){
            super.scrollTo(x, y);
        }
    }

    public boolean isunLuck() {
        return unluck;
    }

    public void setunLuck(boolean luck) {
        this.unluck = luck;
    }
}
