package com.example.user.invetory.sell;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.invetory.R;

public class FragmentCustomDialig extends DialogFragment implements TextWatcher {

    EdittextCallBack callBack;
    int spinnercode;
    private static String KEYCODE = "code";

    @Override
    public void onAttach(Context context) {
        callBack = (EdittextCallBack) context;
        super.onAttach(context);

    }

    public static FragmentCustomDialig newInstance(int code) {

        Bundle args = new Bundle();
        args.putInt(KEYCODE , code);
        FragmentCustomDialig fragment = new FragmentCustomDialig();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        spinnercode = getArguments().getInt(KEYCODE);
    }

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.customlayoutfragmentdialog,container,false);



        TextView tv = v.findViewById(R.id.numberresulttxt);
        AppCompatButton done = v.findViewById(R.id.done);
        final AppCompatEditText code = v.findViewById(R.id.serialnumber);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Please fill field", Toast.LENGTH_SHORT).show();
                }else {
                    getDialog().dismiss();
                }
            }
        });


        if (spinnercode == 1){
            tv.setText("Cheque Number");
        }else if (spinnercode == 2 ){
            tv.setText("TransAction Number");
        }
        code.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        code.addTextChangedListener(this);

        return v;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!s.toString().isEmpty()){
            callBack.CallBackSerialNumberٍ(s.toString());
        }
    }
}
