package com.example.user.invetory.sell;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.example.user.invetory.Cryptor;
import com.example.user.invetory.MainActivity;
import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SellActivity extends AppCompatActivity implements EdittextCallBack {

    private static final String CODEKEY = "Code";
    private static final String POSITION = "Position";
    private static final String CURRENTPRICE = "CurrentPrice";
    private static final String RECYCLERPOSITION = "recyclerPosition";


    Toolbar toolbar;
    TextView totalsale ,totalstock, curentstock , Name  , price;
    AppCompatEditText salestock;
    String stock , stockformat , sale , saleformat , name , stringid;
    Double priceperdouble ,pricetoshow;
    int color ,colorarrow, id , spinnerposition;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    TSnackbar tSnackbar;
    AppCompatButton save;
    Cryptor cryptor;
    Output_files files;
    String Email , currentstr;
    RequestQueue queue;
    StringRequest request , requestTRans;
    private static String KEY = "afsaneh";
    SweetAlertDialog dialog;
    AppCompatSpinner paymentmethod;
    Map<String,String> coderesult;
    AppCompatImageButton right,left;
    //RecyclerView recyclerView;
    AppCompatImageView bsicon;
    AppCompatTextView bstxt;
    CustomViewPager_Sell viewPagerSell;


    String[] methods = {"Cash","Cheque","Card"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        preferences = PreferenceManager.getDefaultSharedPreferences(SellActivity.this);
        editor = preferences.edit();
        int themepos = preferences.getInt("theme",0);



        switch (themepos){
            case 0 :
                color = ContextCompat.getColor(this,R.color.snblue); setTheme(R.style.DefultTheme);
                colorarrow = color;
                break;
            case 1 :
                color = ContextCompat.getColor(this,R.color.snred);setTheme(R.style.RedTheme);
                colorarrow = color;
                break;
            case 2 :
                color = ContextCompat.getColor(this,R.color.sngreen);setTheme(R.style.GreenTheme);
                colorarrow = color;
                break;
            case 3 :
                color = ContextCompat.getColor(this,R.color.night);setTheme(R.style.NightMode);
                colorarrow = ContextCompat.getColor(this,R.color.Tab_background);;
                break;
        }


        setContentView(R.layout.activity_sell);
        toolbar = (Toolbar)findViewById(R.id.toolbarsell);
        totalstock = (TextView) findViewById(R.id.totalstock);
        totalsale = (TextView) findViewById(R.id.totalprice);
        curentstock = (TextView) findViewById(R.id.currentstock);
        salestock = (AppCompatEditText) findViewById(R.id.edittext);
        price = (TextView) findViewById(R.id.price);
        Name = (TextView) findViewById(R.id.name);
        save = (AppCompatButton) findViewById(R.id.save);
        paymentmethod = (AppCompatSpinner) findViewById(R.id.peyment);
        right = (AppCompatImageButton) findViewById(R.id.right);
        left = (AppCompatImageButton) findViewById(R.id.left);
        bsicon = (AppCompatImageView) findViewById(R.id.icon);
        bstxt = (AppCompatTextView) findViewById(R.id.buyorselltxt);
        viewPagerSell = (CustomViewPager_Sell) findViewById(R.id.viewpagersell);

        cryptor = new Cryptor();
        files = new Output_files(this);
        Email = files.read("email");
        coderesult = new HashMap<>();

        tSnackbar = TSnackbar.make(findViewById(android.R.id.content),"input data is greater than sourced", TSnackbar.LENGTH_LONG);
        View view = tSnackbar.getView();
        view.setBackgroundColor(Color.parseColor("#dedede"));
        TextView textView = (TextView) view.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(color);
        right.getDrawable().setColorFilter(colorarrow,PorterDuff.Mode.SRC_IN);
        left.getDrawable().setColorFilter(colorarrow,PorterDuff.Mode.SRC_IN);

        queue = Volley.newRequestQueue(this);


        ViewPagerBS viewPagerBS = new ViewPagerBS(getSupportFragmentManager());

        viewPagerSell.setunLuck(true);

        viewPagerSell.setAdapter(viewPagerBS);

        viewPagerSell.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                salestock.getText().clear();
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });



        ArrayAdapter spinneradapter = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,methods);

        paymentmethod.setAdapter(spinneradapter);

        paymentmethod.setSelection(0, true);
        View v = paymentmethod.getSelectedView();
        ((TextView)v).setTextColor(color);

        paymentmethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                spinnerposition = position;
                ((TextView) view).setTextColor(color);
                coderesult.put(POSITION,String.valueOf(position));
                showAlertDialog(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });



        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_IN);




        Bundle bundle = getIntent().getExtras();
        stock = bundle.getString("stock");
        stockformat = bundle.getString("stockformat");
        sale = bundle.getString("sale");
        saleformat = bundle.getString("saleformat");
        name = bundle.getString("name");
        id = bundle.getInt("id");
        stringid = String.valueOf(id);


        if (bundle != null && stock != null && stockformat != null && sale != null && saleformat != null && name != null ){

            handleSSLHandshake();


                Name.setText(name);

                totalstock.setText(stock+stockformat);
                totalsale.setText(sale+saleformat);
                priceperdouble = PricePerDouble(stock, sale);
                String pricedemo = String.format(Locale.ENGLISH,"%.03f",priceperdouble);



                salestock.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        saletextchange();
                    }
                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }else{
            Toast.makeText(this, "Sorry Somthing Wrong", Toast.LENGTH_SHORT).show();
            finish();
        }

            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog = new SweetAlertDialog(SellActivity.this,SweetAlertDialog.PROGRESS_TYPE);
                    dialog.setTitleText("Please wait...");
                    dialog.setCancelable(false);
                    dialog.show();
                    salestock.setEnabled(false);
                    String stockEncryption = "";
                    if (salestock.getText().toString().trim().length()>0){
                        if (viewPagerSell.getCurrentItem() == 0){
                            stockEncryption = cryptor.Encrypt(String.valueOf(Integer.valueOf(stock) - Integer.valueOf(salestock.getText().toString())),KEY);
                        }else if (viewPagerSell.getCurrentItem() == 1){
                            stockEncryption = cryptor.Encrypt(String.valueOf(Integer.valueOf(stock) + Integer.valueOf(salestock.getText().toString())),KEY);
                        }
                        updaterequest(stockEncryption,Email,stringid);
                        if (coderesult.get(CODEKEY) == null){
                            coderesult.put(CODEKEY,"0");
                        }
                        TransActionReuest(Email,String.valueOf(id),currentstr,String.valueOf(spinnerposition),String.valueOf(0),coderesult.get(CODEKEY),salestock.getText().toString());
                    }else{
                        Toast.makeText(SellActivity.this, "Please fill the fild", Toast.LENGTH_SHORT).show();
                        dialog.dismissWithAnimation();
                        salestock.setEnabled(true);
                    }

                }
            });
    }

    private void showAlertDialog(int position) {
        FragmentCustomDialig dialig = FragmentCustomDialig.newInstance(position);
        dialig.setCancelable(false);
        dialig.show(getSupportFragmentManager(),"fragmentserialnumber");
    }

    private void updaterequest(final String stock , final String Email , final String id  ){

        String url = "http://www.inventory-customer.com/source/SellUpdate.php";


        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismissWithAnimation();
                if (response.trim().equals("Done")){
                    onBackPressed();
                }else {
                    Toast.makeText(SellActivity.this, response, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                salestock.setEnabled(true);
                dialog.dismissWithAnimation();
                dialog = new SweetAlertDialog(SellActivity.this);
                dialog.setTitleText("Check Your Network");
                dialog.show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("stock",stock);
                map.put("email",Email);
                map.put("id",id);
                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis((15)),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }
    private void TransActionReuest(final String Email,final String objectid , final String Price ,
                                   final String paymentmethod , final String buyOrsell, final String transActionSerial , final String sale_amount ){

        String url = "http://inventory-customer.com/source/writeTrans.php";
        final String encPrice = cryptor.Encrypt(Price,KEY);
        final String encpeymentmethod = cryptor.Encrypt(paymentmethod,KEY);
        final String encbuyOrsell = cryptor.Encrypt(buyOrsell,KEY);
        final String enctransActionSerial = cryptor.Encrypt(transActionSerial,KEY);
        final String encSale_amount = cryptor.Encrypt(sale_amount,KEY);

        requestTRans = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SellActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String, String>();
                map.put("email",Email);
                map.put("objectid",objectid);
                map.put("price",encPrice);
                map.put("peyamntmethod",paymentmethod);
                map.put("buyorsell",String.valueOf(viewPagerSell.getCurrentItem()));
                map.put("transaction",enctransActionSerial);
                map.put("saleamount",encSale_amount);

                return map;
            }
        };
        requestTRans.setShouldCache(false);
        requestTRans.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis((15)),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(requestTRans);


    }



    public static void handleSSLHandshake() {

        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null,trustManagers,new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saletextchange() {
        if (salestock.getText().length() > 0){

            Double salestockint = Double.valueOf(salestock.getText().toString());
            Double totalstockdouble = Double.valueOf(stock);
            if (salestockint != null && totalstockdouble != null){
                if (salestockint <= totalstockdouble && viewPagerSell.getCurrentItem() == 0){
                    try{
                        if (viewPagerSell.getCurrentItem() == 0){
                            curentstock.setText(Integer.valueOf(stock) - Integer.valueOf(salestock.getText().toString())+stockformat);
                        }else if (viewPagerSell.getCurrentItem() == 1){
                            curentstock.setText(Integer.valueOf(stock) + Integer.valueOf(salestock.getText().toString())+stockformat);
                        }
                        Double current =   salestockint * Double.valueOf(sale)  ;
                        currentstr = String.format(Locale.ENGLISH,"%.01f",current);
                        price.setText(currentstr+saleformat);

                    }catch (NumberFormatException e){
                        Toast.makeText(this, "Please enter only Integer ", Toast.LENGTH_SHORT).show();
                    }
                }else if (viewPagerSell.getCurrentItem() == 1){
                    curentstock.setText(Integer.valueOf(stock) + Integer.valueOf(salestock.getText().toString())+stockformat);
                    Double current =   salestockint * Double.valueOf(sale)  ;
                    currentstr = String.format(Locale.ENGLISH,"%.01f",current);
                    price.setText(currentstr+saleformat);
                }else {
                    hideKeyboard(SellActivity.this);
                    tSnackbar.show();
                }
            }
        }else {
            curentstock.setText("0");
            price.setText("0");
            totalsale.setText(sale);
        }
    }




    private Double PricePerDouble(String stock, String sale) {

        Double pricePerfloat = Double.valueOf(sale) / Double.valueOf(stock);
        String stringdouble = String.format(Locale.ENGLISH,"%.50f",pricePerfloat);
        Double pricePerfloatnew = Double.valueOf(stringdouble);

        return pricePerfloatnew;
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        editor.putBoolean("tab2",true);
        editor.putBoolean("tab1",true);
        editor.putBoolean("checktab2",false);
        editor.apply();
        finish();
    }

    @Override
    public void CallBackSerialNumberٍ(String code) {
        coderesult.put(CODEKEY,code);
    }

    public class ViewPagerBS extends FragmentPagerAdapter{

        public ViewPagerBS(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            switch (i){
                case 0 : fragemntviewpager fragemntviewpager1 = com.example.user.invetory.sell.fragemntviewpager.newInstance(0); return fragemntviewpager1;
                case 1 : fragemntviewpager fragemntviewpager2 = com.example.user.invetory.sell.fragemntviewpager.newInstance(1); return fragemntviewpager2;
            }

            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


}
