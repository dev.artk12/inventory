package com.example.user.invetory.sell;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.invetory.Output_files;
import com.example.user.invetory.R;

public class SellFactor extends Fragment {

    TextView CompanyName , ObjectName  , TotalPrice , TotalPrice_result , PricePer , PricePer_result , Stock , Stock_result;
    String companyname , objectname  , totalprice_result , priceper_result , stock_result , priceformat , stockformat;

    private static String NAME_KEY ="Name";
    private static String PROCEPER_KEY ="PricePer";
    private static String TOTLAPRICE_KEY ="TotalPrice";
    private static String PRICEFORMAT_KEY ="PriceFormat";
    private static String STOCK_KEY ="Stock";
    private static String STOCKFORMAT_KEY ="StockFormat";
    Output_files files;

    public static SellFactor newInstance(String Name ,String PricePer, String TotalPrice ,String PriceFormat , String Stock , String StockFormat ) {
        SellFactor fragment = new SellFactor();

        Bundle args = new Bundle();
        args.putString(NAME_KEY,Name);
        args.putString(PROCEPER_KEY,PricePer);
        args.putString(TOTLAPRICE_KEY,TotalPrice);
        args.putString(PRICEFORMAT_KEY,PriceFormat);
        args.putString(STOCK_KEY,Stock);
        args.putString(STOCKFORMAT_KEY,StockFormat);

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sellfactor,container,false);
        CompanyName = (TextView) view.findViewById(R.id.company_name);
        ObjectName = (TextView) view.findViewById(R.id.Name);
        Stock = (TextView) view.findViewById(R.id.Stock);
        Stock_result = (TextView) view.findViewById(R.id.description_Stock);
        PricePer = (TextView) view.findViewById(R.id.priceper);
        PricePer_result = (TextView) view.findViewById(R.id.description_priceper);
        TotalPrice = (TextView) view.findViewById(R.id.TotalPrice);
        TotalPrice_result = (TextView) view.findViewById(R.id.description_TotalPrice);
        files = new Output_files(getContext());
        companyname = files.read("name");
        objectname = getArguments().getString(NAME_KEY);
        totalprice_result = getArguments().getString(TOTLAPRICE_KEY);
        priceper_result = getArguments().getString(PROCEPER_KEY);
        stock_result = getArguments().getString(STOCK_KEY);
        priceformat = getArguments().getString(PRICEFORMAT_KEY);
        stockformat = getArguments().getString(STOCKFORMAT_KEY);

        TotalPrice.append(" "+priceformat);
        TotalPrice_result.setText(totalprice_result+priceformat);
        PricePer.append(" "+stockformat);
        PricePer_result.setText(priceper_result+priceformat);;
        Stock_result.setText(stock_result+stockformat);


        CompanyName.setText(companyname);





        return view;
    }
}
