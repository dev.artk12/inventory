package com.example.user.invetory.sell;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.invetory.R;

public class fragemntviewpager extends Fragment {


    private static final String POSITION = "Position";
    String namebs;
    AppCompatTextView txt;
    AppCompatImageView img;
    int position , colorbs , drawblebs;


    public static fragemntviewpager newInstance(int pos) {

        Bundle args = new Bundle();
        args.putInt(POSITION,pos);
        fragemntviewpager fragment = new fragemntviewpager();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(POSITION);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.sellorbuyitem,container,false);

        img = (AppCompatImageView) view.findViewById(R.id.iconbtn);
        txt = (AppCompatTextView) view.findViewById(R.id.buyorselltxt);

        if (position == 0){
            colorbs = ContextCompat.getColor(getContext(),R.color.redcheckedtsignup);
            drawblebs = R.drawable.export_variant;
            namebs = "Sell";
        }else if (position == 1){
            colorbs = ContextCompat.getColor(getContext(),R.color.greencheckedtsignup);
            drawblebs = R.drawable.ic_add_shopping_cart_black_24dp;
            namebs = "Buy";
        }

        img.setImageResource(drawblebs);
        txt.setText(namebs);
        img.getDrawable().setColorFilter(colorbs,PorterDuff.Mode.SRC_IN);


        return view;
    }
}
