package com.example.user.invetory.sell;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.user.invetory.R;

import java.util.ArrayList;
import java.util.List;

public class selladapter extends RecyclerView.Adapter<selladapter.ViewHolder> {

    Context context;
    ArrayList<SellOrBuy> list;
    PositionCallback callback;

    public selladapter(Context context , ArrayList<SellOrBuy> list){

        this.context = context;
        this.list = list;
        callback = (PositionCallback) context;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.sellorbuyitem,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {


        callback.PosCallBack(i);
        viewHolder.icon.setImageResource(list.get(i).getDrawable());
        viewHolder.tv.setText(list.get(i).getName());
        viewHolder.icon.getDrawable().setColorFilter(list.get(i).getColor(),PorterDuff.Mode.SRC_IN);
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatImageButton icon;
        AppCompatTextView tv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = (AppCompatImageButton) itemView.findViewById(R.id.iconbtn);
            tv = (AppCompatTextView) itemView.findViewById(R.id.buyorselltxt);

        }

    }

}

