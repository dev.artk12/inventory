package com.example.user.invetory;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class spinneradapter extends RecyclerView.Adapter<spinneradapter.viewholder>  {


    ArrayList<String> list ;
    EditText text;
    Context context;
    LayoutInflater layoutInflater;
    boolean check;
    SharedPreferences preferences;

    public spinneradapter(boolean check,Context context, EditText editText , ArrayList<String> formas){
        this.list = formas;
        this.text = editText;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.check = check;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    class viewholder extends RecyclerView.ViewHolder{

        public TextView tv;
        public RelativeLayout parent;

        public viewholder(@NonNull View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.formats);
            parent = (RelativeLayout) itemView.findViewById(R.id.parent);
        }
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.spinnerlayoutadapter,viewGroup,false);
        viewholder viewholder = new viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull final viewholder viewholder, int i) {
        viewholder.tv.setText(list.get(i));


        viewholder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setText(viewholder.tv.getText().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
